function clickLink(element) {
	if (element == null)
		return;

	if (element.click) {
		element.click();
	}
	else {
		var clickEvent = document.createEvent('MouseEvents');
		clickEvent.initEvent('click', true, true);
		element.dispatchEvent(clickEvent);
	}
}

function expandAll() {
	var exist = false;
	var treegridList = document.querySelectorAll('.treegrid_cell');
	for (var i = 0; i < treegridList.length; i++) {
		var treenode = treegridList[i];
		var imgList = treenode.querySelectorAll('img');
		for (var j = 0 ; j < imgList.length; j++) {
			var imgtag = imgList[j];
			if (imgtag.src.endsWith('plus.gif')) {
				exist = true;
				clickLink(imgtag);
			}	
		}
	}
	return exist;
}

function checkNetworkComplete() {
	var complete = true;
	var nodevalList = document.querySelectorAll('#nodeval');
	for (var i = 0; i < nodevalList.length; i++) {
		var nodeval = nodevalList[i];
		if (nodeval.innerText.startsWith('Loading')) {
			complete = false;
			break;
		}
	}
	return complete;
}

function endExpandAll() {
	alert('완료');
}

function addFunctions() {
	if (!String.prototype.startsWith) {
	    Object.defineProperty(String.prototype, 'startsWith', {
	        enumerable: false,
	        configurable: false,
	        writable: false,
	        value: function (searchString, position) {
	            position = position || 0;
	             return this.indexOf(searchString, position) === position;
	        }
	    });
	}

	if (!String.prototype.endsWith) {
	    Object.defineProperty(String.prototype, 'endsWith', {
	        enumerable: false,
	        configurable: false,
	        writable: false,
	        value: function (searchString) {
	             return this.indexOf(searchString)+searchString.length === this.length;
	        }
	    });
	}  
}

function expandAndShowAll() {
	addFunctions();

	var exist = expandAll();
	if (!exist) {
		endExpandAll();
		return;
	}

	var timer = setInterval(function(){
		if (checkNetworkComplete()) {
			var exist = expandAll();
			if (!exist) {
				clearInterval(timer);
				endExpandAll();
				return;
			}
		}
	}, 2000);
}
