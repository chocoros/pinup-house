function clickLink(element) {
	if (element == null)
		return;

	if (element.click) {
		element.click();
	}
	else {
		var clickEvent = document.createEvent('MouseEvents');
		clickEvent.initEvent('click', true, true);
		element.dispatchEvent(clickEvent);
	}
}

function checkNetworkComplete(products) {
	console.log('[checkNetworkComplete] length=', products.length);
	var liList = document.querySelector('.quickOrdList').querySelectorAll('li');
	var complete = true;
	for (var i = 0; i < liList.length; i++) {
		if ((i+1) == products.length)
			break;

		var li = liList[i];
		if (!li.hasAttribute('data-code') || !li.hasAttribute('data-outofstock')) {
			var product = products[i];
			console.log(li.getAttribute('data-code') + ', ' + li.getAttribute('data-outofstock'));
			complete = false;
			break;
		}
	}
	return complete;
}

function calculateProductsQty(products) {
	console.log('[calculateProductsQty] products.length=' + products.length);
	var inputPrdQtyList = document.getElementsByName('qty');
	var inputPrdChckList = document.getElementsByName('prdChck');
	var liList = document.querySelector('.quickOrdList').querySelectorAll('li');
	console.log('liList.length = '+ liList.length);

	for (var i = 0; i < products.length; i++) {
		var product = products[i];
		var inputPrdQty = inputPrdQtyList[i];
		var li = liList[i];

		inputPrdQty.value = product['count'];
		if (li.hasAttribute('data-outofstock') && li.getAttribute('data-outofstock') == 'Y') {
			var inputPrdChck = inputPrdChckList[i];
			inputPrdChck.checked = false;
		}
		else {
			$(inputPrdQty).keyup();	
		}

		console.log('inputPrdQty = ' + inputPrdQty.value + " vps:" + product['vps']);
	}

	setTimeout(function(){
		console.log('Ready to finish');
		var changeBasket = document.querySelector('.btnBasicRL');
		clickLink(changeBasket);
	}, 1000);
}

function postAddProducts() {
	console.log('[postAddProducts] globalProductList.length=' + document.body.globalProductList.length);
	var emptyVpsCdCount = 5;
	var needVpsCdCount = document.body.globalProductList.length - emptyVpsCdCount;
	var addProductButton = document.getElementById('addInput');
	for (var i = 0; i < needVpsCdCount; i++) {
    	clickLink(addProductButton);
	}
	console.log('needVpsCdCount = ' + needVpsCdCount);

	var inputVpsCdList = document.getElementsByName('vpsCode');
	console.log('liList.length = '+ inputVpsCdList.length);

	for (var i = 0; i < document.body.globalProductList.length; i++) {
		var product = document.body.globalProductList[i];
		var inputVpsCd = inputVpsCdList[i];

		inputVpsCd.value = product['vps'];
		$(inputVpsCd).keyup();
		console.log('inputVpsCd.value = ' + inputVpsCd.value);
	}

	var timer = setInterval(function(products) {
		if (checkNetworkComplete(products)) {
			clearInterval(timer);
			calculateProductsQty(products);
		}
	}, 1000, document.body.globalProductList);

}

function addProducts(products) {
	var json = JSON.stringify(products);
	console.log('=====================addProducts=====================');
	console.log(json);
	document.body.globalProductList = JSON.parse(json);
	console.log('[addProducts] globalProductList.length=' + document.body.globalProductList.length);
	console.log(document.body.globalProductList);

	// 
	var deleteProduct = document.getElementById('deleteProduct');
	if (deleteProduct) {
		$.cart.delProduct();
	}

	var uiLayerPop_QOrder = document.getElementById('callQuickOrderPopup');
	clickLink(uiLayerPop_QOrder);

	setTimeout(function() {
		postAddProducts();
	}, 1000);
}
