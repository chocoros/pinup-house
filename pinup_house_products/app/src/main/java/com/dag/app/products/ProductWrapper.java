package com.dag.app.products;

public class ProductWrapper {
    public String name;
    public String sku;
    public String vps;
    public String pv;
    public String bv;
    public String price;
    public String brand;
    public String subbrand;
    public String promotion;
    public String imagesrc;
    public String aliasname;
}
