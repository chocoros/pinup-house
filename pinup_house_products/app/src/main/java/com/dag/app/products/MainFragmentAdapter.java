package com.dag.app.products;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainFragmentAdapter extends FragmentPagerAdapter implements  CustomerFragment.OnCartClickListener {

	public final static int MAX_PAGE = 2;
	public final static int PRODUCT_FRAGMENT = 0;
	public final static int CUSTOMER_FRAGMENT = 1;

	private ArrayList<Fragment> fragments;

    public MainFragmentAdapter(FragmentManager fm) {
        super(fm);

        initPageFragments();
    }

    private void initPageFragments() {
    	fragments = new ArrayList<Fragment>();
    	fragments.add(new ProductFragment());
    	fragments.add(new CustomerFragment());

        getCustomerFragment().setOnCartClickListener(this);
    }

    @Override
    public Fragment getItem(int position) {
    	if (position < 0 || position >= MAX_PAGE)
    		return null;

    	return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
    	switch (position) {
    	case PRODUCT_FRAGMENT:
    		return "스마트 제품 검색";
    	case CUSTOMER_FRAGMENT:
    		return "소비자 주문 관리";
    	}
        return "핀업";
    }

    private ProductFragment getProductFragment() {
    	return (ProductFragment)fragments.get(PRODUCT_FRAGMENT);
    }

	private CustomerFragment getCustomerFragment() {
    	return (CustomerFragment)fragments.get(CUSTOMER_FRAGMENT);
    }

    public void setShowcaseViewMode(boolean enabled) {
    	getProductFragment().setShowcaseViewMode(enabled);
    }

    public int getProductCount() {
    	return getProductFragment().getProductCount();
    }

    public void filter(String keyword, int currentItem) {
        if (currentItem == PRODUCT_FRAGMENT)
    	    getProductFragment().filter(keyword);
        else if (currentItem == CUSTOMER_FRAGMENT)
            getCustomerFragment().filter(keyword);
    }

    public List<Product> getSelected() {
    	return getProductFragment().getSelected();
    }

    public void selectProductsByCart(Cart cart, boolean clean) {
    	getProductFragment().selectProductsByCart(cart, clean);
    }

    public void decreaseTextSize() {
    	getProductFragment().decreaseTextSize();
    }

    public void increaseTextSize() {
    	getProductFragment().increaseTextSize();
    }

    public void updateCustomerList() {
    	getCustomerFragment().updateCustomerList();
    }

    public List<String> getPersonNameList() {
        return getCustomerFragment().getPersonNameList();
    }

    public Customer getPerson(Cart cart) {
        return getCustomerFragment().getPerson(cart);
    }

    public Customer getPerson(String personName) { return getCustomerFragment().getPerson(personName); }

    public void expandAllCustomer(boolean expand) {
        getCustomerFragment().expandAllCustomer(expand);
    }

    @Override
    public void onCartClick(final Cart cart) {
        Activity context = getProductFragment().getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        TextView textView = (TextView) context.getLayoutInflater().inflate(android.R.layout.simple_list_item_1, null);
        textView.setText(R.string.info_select_by_cart);

        builder.setNegativeButton(R.string.info_reorder_clean, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectProductsByCart(cart, true);
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(R.string.info_reorder_addable, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectProductsByCart(cart, false);
                dialog.dismiss();
            }
        });

        builder.setView(textView).create().show();
    }
}