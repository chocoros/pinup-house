package com.dag.app.products;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class Customer extends SugarRecord<Customer> {
    public String name;
    public String memo0;
    public String memo1;
    public String memo2;
    public String memo3;
    public String memo4;
    public String memo5;
    public String memo6;
    public String memo7;
    public String memo8;
    public String memo9;

    @Ignore
    public int MEMO_LENGTH = 10;
    @Ignore
    private ArrayList<String> memos;
    @Ignore
    public int monthlyPV;
    @Ignore
    public int monthlyBV;
    @Ignore
    public long lastDate;
    @Ignore
    public ArrayList<Cart> cartList;
    @Ignore
    public ArrayList<Cart> totalCartList;
    @Ignore
    public boolean willBeDeleted;

    public Customer() {
        name = "";
        memo0 = "";
        memo1 = "";
        memo2 = "";
        memo3 = "";
        memo4 = "";
        memo5 = "";
        memo6 = "";
        memo7 = "";
        memo8 = "";
        memo9 = "";
        memos = new ArrayList<String>();
        monthlyPV = 0;
        monthlyBV = 0;
        lastDate = 0;
        cartList = new ArrayList<Cart>();
        totalCartList = new ArrayList<Cart>();
        willBeDeleted = false;
    }

    public void initWith(String name, List<String> memos) {
        this.name = name;
        this.memos.clear();
        if (memos == null)
            return;

        this.memos.addAll(memos);

        int i = 0;
        if (memos.size() > i)
            memo0 = memos.get(i++);
        if (memos.size() > i)
            memo1 = memos.get(i++);
        if (memos.size() > i)
            memo2 = memos.get(i++);
        if (memos.size() > i)
            memo3 = memos.get(i++);
        if (memos.size() > i)
            memo4 = memos.get(i++);
        if (memos.size() > i)
            memo5 = memos.get(i++);
        if (memos.size() > i)
            memo6 = memos.get(i++);
        if (memos.size() > i)
            memo7 = memos.get(i++);
        if (memos.size() > i)
            memo8 = memos.get(i++);
        if (memos.size() > i)
            memo9 = memos.get(i++);
    }

    public ArrayList<String> getMemos() {
        memos.clear();
        memos.add(memo0);
        memos.add(memo1);
        memos.add(memo2);
        memos.add(memo3);
        memos.add(memo4);
        memos.add(memo5);
        memos.add(memo6);
        memos.add(memo7);
        memos.add(memo8);
        memos.add(memo9);
        return memos;
    }

    public List<Cart> getCartList() {
        return cartList;
    }

    public List<Cart> getTotalCartList() {
        return totalCartList;
    }

    public Cart getCart(int index) {
        if (index < 0 || index >= cartList.size())
            return null;

        return cartList.get(index);
    }

    public void updateMonthlyInfo() {
        Calendar thisMonthStart = Calendar.getInstance(Locale.getDefault());
        Calendar nextMonthStart = Calendar.getInstance(Locale.getDefault());
        resetCalendarToFirstDat(thisMonthStart);
        resetCalendarToFirstDat(nextMonthStart);
        nextMonthStart.add(Calendar.MONTH, 1);

        long start = thisMonthStart.getTimeInMillis();
        long end = nextMonthStart.getTimeInMillis();

        // add pv
        this.monthlyPV = 0;
        this.monthlyBV = 0;
        this.lastDate = 0;
        for (Cart cart : totalCartList) {
            long now = cart.getDate().getTime();
            if (this.lastDate < now)
                this.lastDate = now;

            if (start <= now && now < end) {
                this.monthlyPV += cart.getPv();
                this.monthlyBV += cart.getBv();
            }
        }
    }

    public void addCart(Cart cart) {
        cartList.add(cart);
        totalCartList.add(cart);

        // update monthly pv & bv
        Calendar thisMonthStart = Calendar.getInstance(Locale.getDefault());
        Calendar nextMonthStart = Calendar.getInstance(Locale.getDefault());
        resetCalendarToFirstDat(thisMonthStart);
        resetCalendarToFirstDat(nextMonthStart);
        nextMonthStart.add(Calendar.MONTH, 1);

        long start = thisMonthStart.getTimeInMillis();
        long end = nextMonthStart.getTimeInMillis();
        long now = cart.getDate().getTime();
        if (start <= now && now < end) {
            this.monthlyPV += cart.getPv();
            this.monthlyBV += cart.getBv();
        }
    }

    public void deleteAllCart() {
        cartList.clear();
        for (Cart cart : totalCartList) {
           cart.delete();
        }
        totalCartList.clear();
    }

    public Cart removeCart(int index) {
        Cart cart = cartList.remove(index);
        totalCartList.remove(cart);

        // update monthly pv & bv
        Calendar thisMonthStart = Calendar.getInstance(Locale.getDefault());
        Calendar nextMonthStart = Calendar.getInstance(Locale.getDefault());
        resetCalendarToFirstDat(thisMonthStart);
        resetCalendarToFirstDat(nextMonthStart);
        nextMonthStart.add(Calendar.MONTH, 1);

        long start = thisMonthStart.getTimeInMillis();
        long end = nextMonthStart.getTimeInMillis();
        long now = cart.getDate().getTime();
        if (start <= now && now < end) {
            this.monthlyPV -= cart.getPv();
            this.monthlyBV -= cart.getBv();
        }
        return cart;
    }

    public void loadProductList() {
        for (Cart cart : totalCartList) {
            if (cart.getProductList().size() == 0) {
                cart.loadProductList();
            }
        }
    }

    public void filter(String keyword) {
        cartList.clear();
        if (keyword == null || keyword.length() == 0) {
            cartList.addAll(totalCartList);
            return;
        }

        // 이름 검색도 추가 요청
        if (name.contains(keyword)) {
            cartList.addAll(totalCartList);
            return;
        }

        for (Cart cart : totalCartList) {
            // [2016.10.22] 로딩 성능 개선 후 검색 기능이 빠져서 검색할 때 로딩하도록 한다 성능 테스트 필요
            if (cart.getBrief().contains(keyword)) {
                cartList.add(cart);
            }
            else if (cart.getTitle().contains(keyword)) {
                cartList.add(cart);
            }
            else {
                if (cart.getProductList().size() == 0) {
                    cart.loadProductList();
                }

                for (CartProductListRecord product : cart.getProductList()) {
                    if (product.getName() == null) {
                        if (cart.getBrief().contains(keyword)) {
                            cartList.add(cart);
                            break;
                        }
                    }
                    if (product.filtered(keyword)) {
                        cartList.add(cart);
                        break;
                    }
                }
            }
        }
    }

    private void resetCalendarToFirstDat(Calendar cal) {
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
    }

    public void changeCustomer(Customer sameCustomer) {
        for (Cart cart : totalCartList) {
            cart.setPersonId(sameCustomer.getId());
            cart.save();

            sameCustomer.addCart(cart);
        }

        Collections.sort(getTotalCartList(), new Comparator<Cart>() {
            @Override
            public int compare(Cart lhs, Cart rhs) {
                return rhs.getDate().compareTo(lhs.getDate());
            }
        });
        Collections.sort(getCartList(), new Comparator<Cart>() {
            @Override
            public int compare(Cart lhs, Cart rhs) {
                return rhs.getDate().compareTo(lhs.getDate());
            }
        });

        sameCustomer.updateMonthlyInfo();
        cartList.clear();
        totalCartList.clear();
    }
}
