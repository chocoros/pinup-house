package com.dag.app.products;

public class CashbackCalculator {

    final static int PERCENT_ZERO = 0;
    final static int PERCENT_THREE = 3;
    final static int PERCENT_SIX = 6;
    final static int PERCENT_NINE = 9;
    final static int PERCENT_TWELVE = 12;
    final static int PERCENT_FIFTEEN = 15;
    final static int PERCENT_TWENTYONE = 21;

    final static int PV_THREE = 200000;
    final static int PV_SIX = 600000;
    final static int PV_NINE = 1200000;
    final static int PV_TWELVE = 2400000;
    final static int PV_FIFTEEN = 4000000;
    final static int PV_TWENTYONE = 10000000;

    static int getPercent(int pv) {
        if (PV_TWENTYONE <= pv)
            return PERCENT_TWENTYONE;
        else if (PV_FIFTEEN <= pv)
            return PERCENT_FIFTEEN;
        else if (PV_TWELVE <= pv)
            return PERCENT_TWELVE;
        else if (PV_NINE <= pv)
            return PERCENT_NINE;
        else if (PV_SIX <= pv)
            return PERCENT_SIX;
        else if (PV_THREE <= pv)
            return PERCENT_THREE;
        return PERCENT_ZERO;
    }

    static int calculate1stCachback(int pv, int bv) {
        int percent = getPercent(pv);
        return percent * bv / 100;
    }
}
