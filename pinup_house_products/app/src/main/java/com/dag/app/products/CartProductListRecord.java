package com.dag.app.products;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.List;
import java.util.Locale;

public class CartProductListRecord extends SugarRecord<CartProductListRecord> {
    private long orderid;
    private String sku;
    private int count;
    private String name;
    private int vps;
    private int pv;
    private int bv;
    private int price;
    private String brand;
    private String category1;
    private String category2;
    private String category3;
    private String alias;

    // custom information
    @Ignore
    private String compareName;
    @Ignore
    private String compareBrand;
    @Ignore
    private String compareSubbrand;
    @Ignore
    private String compareAlias;

    public CartProductListRecord() {
    }

    public void initWith(long orderid, String sku, int count, String name, int vps, int pv, int bv, int price, String brand, String subbrand, String promotion, String imagesrc, String alias){
        this.orderid = orderid;
        this.sku = sku;
        this.count = count;
        this.name = name;
        this.vps = vps;
        this.pv = pv;
        this.bv = bv;
        this.price = price;
        this.brand = brand;
        this.category1 = subbrand;
        this.category2 = promotion;
        this.category3 = imagesrc;
        this.alias = alias;
    }

    public String getSKU() {
        return sku;
    }
    public int getCount() {
        return count;
    }

    public long getOrderId() { return orderid; }

    public String getName() { return name; }

    private void makeOldItem() {
        List<Product> list = Product.find(Product.class, "sku = ?", this.sku);
        if (list.size() == 0)
            return;

        Product product = list.get(0);
        this.name = product.getName();
        this.vps = product.getVPS();
        this.pv = product.getPV();
        this.bv = product.getBV();
        this.price = product.getPrice();
        this.brand = product.getBrand();
        this.category1 = product.getSubbrand();
        this.category2 = product.getPromotion();
        this.category3 = product.getImageSrc();
        this.alias = product.getAlias();

        this.save();
    }

    public void makeComparisonData() {
        if (this.name == null) {
            makeOldItem();
        }

        this.compareName = name == null ? "" : name.toLowerCase(Locale.getDefault()).replaceAll("\\s", "");
        this.compareBrand = brand == null ? "" : brand.toLowerCase(Locale.getDefault()).replaceAll("\\s", "");
        this.compareSubbrand = category1 == null ? "" : category1.toLowerCase(Locale.getDefault()).replaceAll("\\s", "");
        this.compareAlias = alias == null ? "" : alias.toLowerCase(Locale.getDefault()).replaceAll("\\s", "");

        //Log.d("Product", "[name] " + compareName + ", " + compareBrand + ", " + compareCategory1 + ", " + compareCategory2 + ", " + compareCategory3);
        //Log.d("Product", "[alias] " + compareAlias);
    }

    public boolean filtered(String keyword) {
        if (this.name == null)
            return false;

        if (this.compareName != null && this.compareName.contains(keyword))
            return true;
        if (this.compareAlias != null && this.compareAlias.contains(keyword))
            return true;
        if (this.compareBrand != null && this.compareBrand.contains(keyword))
            return true;
        if (this.compareSubbrand != null && this.compareSubbrand.contains(keyword))
            return true;

        return false;
    }
}
