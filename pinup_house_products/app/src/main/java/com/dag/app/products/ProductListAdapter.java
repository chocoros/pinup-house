package com.dag.app.products;

import android.content.Context;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ProductListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<Product> products = null;
    private ArrayList<Product> filteredList = null;

    private MyOnClickListener onClickListener = new MyOnClickListener();

    private TextView textviewTotalPV;
    private TextView textviewTotalBV;
    private TextView textviewTotalPrice;
    private TextView textviewTotalCashback;
    private DecimalFormat decimalFormat;

    private float textScale = 1.0f;
    final private float textScaleUnit = 0.1f;
    final private float minimumTextScale = 1.0f;
    final private float maximumTextScale = 1.8f;

    float total_pv_size;
    float total_bv_size;
    float total_price_size;
    float total_cashback_size;

    private Context context;
    private boolean isShowCaseViewMode;

    private static final int DEFAULT_DISK_USAGE_BYTES = 25 * 1024 * 1024;
    private static final String DEFAULT_CACHE_DIR = "images";

    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    public ProductListAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);

        this.filteredList = new ArrayList<Product>();
        this.products = new ArrayList<Product>();

        total_pv_size = total_bv_size = total_price_size = total_cashback_size = 0;
        isShowCaseViewMode = false;

        requestQueue = Volley.newRequestQueue(context);
        imageLoader = new ImageLoader(requestQueue, new BitmapLruCache());
    }

    public void setProductList(List<Product> products) {
        this.products.clear();
        this.filteredList.clear();

        this.products.addAll(products);
        this.filteredList.addAll(this.products);

        notifyDataSetChanged();
    }

    public void setShowcaseViewMode(boolean enabled) {
        this.isShowCaseViewMode = enabled;
        notifyDataSetChanged();
    }

    public void clearCache() {
        if (requestQueue != null) {
            requestQueue.getCache().clear();
        }
    }

    public class ViewHolder {
        NetworkImageView image;
        TextView name;
        TextView pv_bv;
        TextView price_vps;
        TextView count;

        Button add;
        Button remove;

        float name_size;
        float pv_bv_size;
        float price_vps_size;

        public ViewHolder() {
            name_size = pv_bv_size = price_vps_size = 0;
        }
    }

    public int getCount() {
        final int MINIMUM_ROW = 1;
        if (isShowCaseViewMode && MINIMUM_ROW < filteredList.size()) {
            return  MINIMUM_ROW;
        }
        return filteredList.size();
    }

    public Product getItem(int position) {
        return filteredList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.listitem_product, null);

            holder.image = (NetworkImageView) view.findViewById(R.id.imageview_product_image);
            holder.name = (TextView) view.findViewById(R.id.textview_product_name);
            holder.pv_bv = (TextView) view.findViewById(R.id.textview_product_pv_bv);
            holder.price_vps = (TextView) view.findViewById(R.id.textview_product_price_vps);
            holder.count= (TextView) view.findViewById(R.id.textview_product_count);

            // set event listener
            // Button
            holder.add = (Button)view.findViewById(R.id.button_product_add);
            holder.remove = (Button)view.findViewById(R.id.button_product_remove);

            holder.add.setOnClickListener(onClickListener);
            holder.remove.setOnClickListener(onClickListener);

            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        // set product information
        Product item = filteredList.get(position);
//        String URL = String.format("http://www.abnkorea.co.kr/images/shopping/product/product_s_img/s%s.jpg", String.valueOf(item.getVPS()));
        String URL = item.getImageSrc();
        if (URL != null && !URL.startsWith("http")) {
            URL = "file:///android_asset/images/" + item.getImageSrc();
        }

        holder.image.setImageUrl(URL, imageLoader);
        holder.name.setText(item.getName());
        holder.pv_bv.setText(String.format("PV: %s BV: %s", getDecimalFormat(item.getPV()), getDecimalFormat(item.getBV())));

        String priceTitle = view.getContext().getString(R.string.price);
        String currency = view.getContext().getString(R.string.currency);
        holder.price_vps.setText(String.format("%s: %s%s [%d]", priceTitle, getDecimalFormat(item.getPrice()), currency, item.getVPS()));

        String count = String.valueOf(item.getCount());
        holder.count.setText(count);

        holder.add.setTag(item);
        holder.add.setTag(holder.count.getId(), holder.count);
        holder.remove.setTag(item);
        holder.remove.setTag(holder.count.getId(), holder.count);
        setTextSize(holder);
        return view;
    }

    private void setTextSize(ViewHolder holder) {
        if (total_pv_size == 0)
            total_pv_size = textviewTotalPV.getTextSize();
        if (total_bv_size == 0)
            total_bv_size = textviewTotalBV.getTextSize();
        if (total_price_size == 0)
            total_price_size = textviewTotalPrice.getTextSize();
        if (total_cashback_size == 0)
            total_cashback_size = textviewTotalCashback.getTextSize();

        if (holder.name_size == 0)
            holder.name_size = holder.name.getTextSize();
        if (holder.pv_bv_size == 0)
            holder.pv_bv_size = holder.pv_bv.getTextSize();
        if (holder.price_vps_size == 0)
            holder.price_vps_size = holder.price_vps.getTextSize();

        holder.name.setTextSize(TypedValue.COMPLEX_UNIT_PX, holder.name_size * textScale);
        holder.pv_bv.setTextSize(TypedValue.COMPLEX_UNIT_PX, holder.pv_bv_size * textScale);
        holder.price_vps.setTextSize(TypedValue.COMPLEX_UNIT_PX, holder.price_vps_size * textScale);

        textviewTotalPV.setTextSize(TypedValue.COMPLEX_UNIT_PX, total_pv_size * textScale);
        textviewTotalBV.setTextSize(TypedValue.COMPLEX_UNIT_PX, total_bv_size * textScale);
        textviewTotalPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, total_price_size * textScale);
        textviewTotalCashback.setTextSize(TypedValue.COMPLEX_UNIT_PX, total_cashback_size * textScale);
    }

    public void decreaseTextSize() {
        textScale -= textScaleUnit;
        if (textScale < minimumTextScale)
            textScale = minimumTextScale;

        notifyDataSetChanged();
    }

    public void increaseTextSize() {
        textScale += textScaleUnit;
        if (textScale > maximumTextScale)
            textScale = maximumTextScale;

        notifyDataSetChanged();
    }

    public void filter(String keyword) {
        keyword = keyword.toLowerCase(Locale.getDefault());
        filteredList.clear();

        if (keyword.length() == 0) {
            filteredList.addAll(products);
        }
        else {
            keyword = keyword.replaceAll("\\s", "");

			int separatorIndex = keyword.indexOf(":");
			if (separatorIndex >= 0) {
				try {
					String lowString = keyword.substring(0, separatorIndex);
					String highString = keyword.substring(separatorIndex+1);
					int low = -1;
					int high = -1;
					if (lowString.length() > 0)
						low = Integer.parseInt(lowString);
					if (highString.length() > 0)
						high = Integer.parseInt(highString);

					for (Product item : products) {
						if (item.filteredWithPrice(low, high)) {							filteredList.add(item);
						}
					}
				} catch (Exception e) {
					filteredList.addAll(products);
				}
			}
			else {
            	for (Product item : products) {
                	if (item.filtered(keyword)) {
                    	filteredList.add(item);
                	}
            	}
			}
        }

        notifyDataSetChanged();
    }

    public void setProductTotalTextView(View totalView) {
        this.textviewTotalPV = (TextView)totalView.findViewById(R.id.textview_product_total_pv);
        this.textviewTotalBV = (TextView)totalView.findViewById(R.id.textview_product_total_bv);
        this.textviewTotalPrice = (TextView)totalView.findViewById(R.id.textview_product_total_price);
        this.textviewTotalCashback = (TextView)totalView.findViewById(R.id.textview_product_total_cashback);
    }

    private void updateProductCount(int increment, Product item, TextView textviewCount) {
        int count = item.getCount() + increment;
        if (count < 0)
            count = 0;
        item.setCount(count);

        textviewCount.setText(String.valueOf(count));

        updateProductTotal();
    }

    public void updateProductTotal() {
        int pv = 0;
        int bv = 0;
        int price = 0;
        for (Product item : products) {
            if (item.getCount() > 0) {
                pv += item.getPV() * item.getCount();
                bv += item.getBV() * item.getCount();
                price += item.getPrice() * item.getCount();;
            }
        }

        String totalPV = String.format("%s: %s", loadString(R.string.pv), getDecimalFormat(pv));
        String totalBV = String.format("%s: %s", loadString(R.string.bv), getDecimalFormat(bv));
        String totalPrice = String.format("%s: %s%s", loadString(R.string.price), getDecimalFormat(price),loadString(R.string.currency));
        String totalCashback = String.format("%s: %s%s", loadString(R.string.cashback), getDecimalFormat(CashbackCalculator.calculate1stCachback(pv, bv)), loadString(R.string.currency));

        this.textviewTotalPV.setText(totalPV);
        this.textviewTotalBV.setText(totalBV);
        this.textviewTotalPrice.setText(totalPrice);
        this.textviewTotalCashback.setText(totalCashback);
    }

    private String getDecimalFormat(int number) {
        if (decimalFormat == null)
            decimalFormat = new DecimalFormat("###,###,###");
        return decimalFormat.format(number);
    }

    private String loadString(int id) {
        return this.textviewTotalPV.getContext().getString(id);
    }

    private class MyOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_product_add:
                {
                    Product item = (Product)v.getTag();
                    TextView textview = (TextView)v.getTag(R.id.textview_product_count);
                    updateProductCount(1, item, textview);
                    break;
                }
                case R.id.button_product_remove:
                {
                    Product item = (Product)v.getTag();
                    TextView textview = (TextView)v.getTag(R.id.textview_product_count);
                    updateProductCount(-1, item, textview);
                    break;
                }
                default:
                    break;
            }
        }
    }

    public void showSelected(boolean onlySelected) {
        filteredList.clear();

        if (!onlySelected) {
            filteredList.addAll(products);
        }
        else {
            for (Product item : products) {
                if (item.getCount() > 0) {
                    filteredList.add(item);
                }
            }
        }

        notifyDataSetChanged();
    }

    public void deletedSelected() {
        for (Product item : products) {
            if (item.getCount() > 0) {
                item.setCount(0);
            }
        }

        updateProductTotal();
        notifyDataSetChanged();
    }

    public List<Product> getSelected() {
        ArrayList<Product> selectedList = new ArrayList<Product>();
        for (Product item : products) {
            if (item.getCount() > 0) {
                selectedList.add(item);
            }
        }
        return selectedList;
    }

    public boolean selectProductsByCart(Cart cart, boolean clean) {
        if (cart.loadProductList() == false)
            return false;

        if (clean)
            deletedSelected();

        List<CartProductListRecord> list = cart.getProductList();
        for (CartProductListRecord item : list) {
            for (Product product : products) {
                if (product.getSKU().equals(item.getSKU()) || product.getName().equalsIgnoreCase(item.getName())) {
                    if (clean)
                        product.setCount(item.getCount());
                    else
                        product.setCount(product.getCount() + item.getCount());
                }
            }
        }

        updateProductTotal();
        notifyDataSetChanged();
        return true;
    }
}