package com.dag.app.products;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dag.app.products.util.IabResult;
import com.dag.app.products.util.Purchase;
import com.dag.web.products.myApi.MyApi;
import com.dag.web.products.myApi.model.ProductList;
import com.dag.web.products.myApi.model.VersionInfo;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.googleapis.services.GoogleClientRequestInitializer;
import com.google.common.primitives.Booleans;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.SugarConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.sql.SQLDataException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Database{
    private static final String TAG = "Database";

    private final String DBPATH_INSDCARD = "PinupHouse";
    private ArrayList<Database.DataBaseListener> listeners;
    private static Database singleton;

    private final String PREF_NAME = "pref";
    private String PREF_FORCE_TO_UPDATE_VERSION_CODE = "forceToUpdateVersionCode";
    private int forceToUpdateVersionCode = 25;

    private MyApi myApiService = null;

    public interface DataBaseListener {
        public void onProgressMessageChanged(int id);
        public void onProductListCreated(List<Product> list, boolean clearCache);
        public void onRequestGoogleApiClientConnection(boolean isSave, boolean isRestore);
        public void onVersionInfoReceived(VersionInfo info);
    }

    public static Database getInstance() {
        if (singleton == null)
            singleton = new Database();
        return singleton;
    }

    private Database() {
        listeners = new ArrayList<Database.DataBaseListener>();
    }

    public void setOnDataBaseListener(DataBaseListener listener) {
        this.listeners.add(listener);
    }

    // apk와 제품 목록을 업데이트 하기 위해 버전 정보를 가져온다.
    public void checkVersion(final Context context) {
//        new GetVersionAsyncTask().execute();

        RequestQueue queue = Volley.newRequestQueue(context);
        final String url = "https://us-central1-pinup-44ea2.cloudfunctions.net/getOldVersions";

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.d("Response", response.toString());

                        try {
                            VersionInfo versionInfo = new VersionInfo();
                            versionInfo.setVersionCode(response.getInt("versionCode"));
                            versionInfo.setProductVersion(Integer.parseInt(response.getString("productVersion")));

                            Log.i(TAG, "versionCode: " + versionInfo.getVersionCode());
                            Log.i(TAG, "productVersion: " + versionInfo.getProductVersion());

                            for (Database.DataBaseListener listener : listeners) {
                                listener.onVersionInfoReceived(versionInfo);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        );

        // add it to the RequestQueue
        queue.add(getRequest);
    }

    // called once
    public void requestProductList(final Context context) {
        if(myApiService == null) {  // Only do this once
            MyApi.Builder builder = new MyApi.Builder(AndroidHttp.newCompatibleTransport(),
                    new AndroidJsonFactory(), null)
                    // options for running against local devappserver
                    // - 10.0.2.2 is localhost's IP address in Android emulator
                    // - turn off compression when running against local devappserver
//                        .setRootUrl("http://10.0.3.2:8080/_ah/api/")
                    .setRootUrl("https://pinup-house-products.appspot.com/_ah/api/")
                    .setGoogleClientRequestInitializer(new GoogleClientRequestInitializer() {
                        @Override
                        public void initialize(AbstractGoogleClientRequest<?> abstractGoogleClientRequest) throws IOException {
                            abstractGoogleClientRequest.setDisableGZipContent(true);
                        }
                    });
            // end options for devappserver

            myApiService = builder.build();
        }

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                if (existDBFileInPackage(context)) {
                    return true;
                }
                else {
                    // [2016.10.04] 카드에 있는 것은 가져오지 말고 드라이브로부터 백업하겠다.
//                    if (existDBFileInSDCard(context))
//                        return copyDBFileFromSDCard(context);  // restore
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                    loadProductListDirect(context);
                }
                // restoreDBFileFromDrive case
                else {
                    for (Database.DataBaseListener listener : listeners) {
                        listener.onProgressMessageChanged(R.string.message_restoring_db);
                        listener.onRequestGoogleApiClientConnection(false, true);
                    }
                }
            }
        }.execute();
    }

    public void loadProductListDirect(Context context) {
        for (Database.DataBaseListener listener : listeners) {
            listener.onProgressMessageChanged(R.string.loading);
        }

        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
        int versionCode = pref.getInt(PREF_FORCE_TO_UPDATE_VERSION_CODE, 0);
        if (versionCode == 0 || versionCode < forceToUpdateVersionCode) {
        }
        else {
            long count = Product.count(Product.class, null, null);
            if (count > 0) {
                loadProductList();
                return;
            }
        }
        updateProductList(context);
    }

    public boolean existDBFileInPackage(Context context) {
        File dbFile = context.getDatabasePath(SugarConfig.getDatabaseName(context));
        return dbFile.exists();
    }

    public boolean existDBFileInSDCard(Context context) {
        File sdcard = Environment.getExternalStorageDirectory();
        if (sdcard.canWrite()) {
            File backupDir = new File(sdcard, DBPATH_INSDCARD);
            File dbFile = new File(backupDir, SugarConfig.getDatabaseName(context));
            return dbFile.exists();
        }
        return false;
    }

    public boolean copyDBFileFromSDCard(Context context) {
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();
            if (sdcard.canWrite()) {
                File backupDir = new File(sdcard, DBPATH_INSDCARD);
                File backupDBFile = new File(backupDir, SugarConfig.getDatabaseName(context));
                File dbFile = new File(context.getDatabasePath(SugarConfig.getDatabaseName(context)).getAbsolutePath());

                dbFile.getParentFile().mkdir();
                FileChannel src = new FileInputStream(backupDBFile).getChannel();
                FileChannel dst = new FileOutputStream(dbFile).getChannel();

                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean copyDBFileToSDCard(Context context) {
        try {
            File sdcard = Environment.getExternalStorageDirectory();
            if (sdcard.canWrite()) {
                File backupDir = new File(sdcard, DBPATH_INSDCARD);
                backupDir.mkdir();

                File backupDBFile = new File(backupDir, SugarConfig.getDatabaseName(context));
                File dbFile = context.getDatabasePath(SugarConfig.getDatabaseName(context));
                if (dbFile.lastModified() > backupDBFile.lastModified()) {
                    FileChannel src = new FileInputStream(dbFile).getChannel();
                    FileChannel dst = new FileOutputStream(backupDBFile).getChannel();

                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean restoreDBFileFromDrive() {
        return false;
    }

    private void loadProductList() {
        new LoadProductAsyncTask().execute();
    }

    public void updateProductList(final Context context) {
//        new EndpointsAsyncTask().execute(new Pair<Context, String>(context, "NEW ITEMS ARRIVAL"));
        RequestQueue queue = Volley.newRequestQueue(context);

        final String url = "https://us-central1-pinup-44ea2.cloudfunctions.net/getOldProducts";

// prepare the Request
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
//                        Log.d("Response", response.toString());
                        List<Product> products = getProductList(context, response.toString());
                        if (products.size() > 0) {
                            Product.deleteAll(Product.class);
                            Product.saveInTx(products);
                            recordUpdateDate(products.size());

                            for (Database.DataBaseListener listener : listeners) {
                                boolean clearCache = true;
                                listener.onProductListCreated(products, clearCache);
                            }

                            if (products.size() > 0) {
                                Toast.makeText(context, R.string.finish_update_new_products, Toast.LENGTH_LONG).show();

                                SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
                                int versionCode = pref.getInt(PREF_FORCE_TO_UPDATE_VERSION_CODE, 0);
                                if (versionCode == 0 || versionCode < forceToUpdateVersionCode) {
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putInt(PREF_FORCE_TO_UPDATE_VERSION_CODE, forceToUpdateVersionCode);
                                    editor.commit();
                                }
                            }
                            else {
                                Toast.makeText(context, R.string.fail_update_new_products, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        );

        // add it to the RequestQueue
        queue.add(getRequest);
    }


    public ArrayList<Product> getProductList(Context context, String data) {
        ArrayList<Product> list = new ArrayList<Product>();
        try {
            GsonBuilder builder = new GsonBuilder();
            builder.setPrettyPrinting();
            Gson gson = builder.create();
            ProductWrapper[] products = gson.fromJson(data, ProductWrapper[].class);
            //int count = 0;
            for (ProductWrapper i : products) {
                //Log.d(TAG, "[Product][" + ++count + "]" + i.name + ", " + i.sku + ", " + i.vps + ", " + i.pv + ", " + i.bv + ", " + i.price + ", " + i.brand + ", " + i.category1 + ", " + i.category2 + ", " + i.category3 + ", " + i.alias + " ...");
                Product item = new Product();
                item.initWith(i.name, i.vps, i.pv, i.bv, i.price, i.sku, i.brand, i.subbrand, i.promotion, i.imagesrc, i.aliasname);
                list.add(item);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private void resetCalendarHourBelow(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
    }

    private void recordUpdateDate(int productSize) {
        // record update date
        KeyValue keyvalue = null;

        try {
            List<KeyValue> list = KeyValue.find(KeyValue.class, "key = ?", KeyValue.KEY_UPDATE_PRODUCTS);
            if (list.size() > 0) {
                keyvalue = list.get(0);
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

        if (keyvalue == null)
            keyvalue = new KeyValue();

        keyvalue.initWith(KeyValue.KEY_UPDATE_PRODUCTS, String.valueOf(productSize), KeyValue.DateFormat.format(new Date()));
        keyvalue.save();
    }

    private Date getLastUpdateDate() {
        Date date = null;
        try {
            List<KeyValue> list = KeyValue.find(KeyValue.class, "key = ?", KeyValue.KEY_UPDATE_PRODUCTS);
            KeyValue keyvalue = null;
            if (list.size() > 0) {
                keyvalue = list.get(0);
                try {
                    date = KeyValue.DateFormat.parse(keyvalue.getUpdateDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return date;
    }

    public boolean isInExpiry() {
        Date expiry = null;
        try {
            List<KeyValue> list = KeyValue.find(KeyValue.class, "key = ?", KeyValue.KEY_SUBS_EXPIRY);
            KeyValue keyvalue = null;
            if (list.size() > 0) {
                keyvalue = list.get(0);
                try {
                    expiry = KeyValue.DateFormat.parse(keyvalue.getValue());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }

        if (expiry == null)
            return false;

        Date now = new Date();
        boolean inExpiry = expiry.after(now);
        return inExpiry;
    }

    public boolean canUpdateProductsWithFreeUser() {
        Date lastUpdateDate = getLastUpdateDate();
        if (lastUpdateDate == null)
            return true;            // 한번도 받은 적이 없을 경우 업데이트 진행

        // 3 months
        int duration_month = 3;
        int day_of_month = 1;
        Calendar targetDate = Calendar.getInstance(Locale.getDefault());
        targetDate.setTime(lastUpdateDate);

        targetDate.add(Calendar.MONTH, duration_month);

        targetDate.set(Calendar.DAY_OF_MONTH, day_of_month);
        resetCalendarHourBelow(targetDate);

        Calendar now = Calendar.getInstance(Locale.getDefault());
        resetCalendarHourBelow(now);

        boolean canUpdate = !targetDate.after(now);

        Log.d(TAG, "[canUpdateProductsWithFreeUser] targetDate = " + KeyValue.DateFormat.format(targetDate.getTime()));
        Log.d(TAG, "[canUpdateProductsWithFreeUser] now = " + KeyValue.DateFormat.format(now.getTime()));
        Log.d(TAG, "[canUpdateProductsWithFreeUser] canUpdate = " + canUpdate);
        return canUpdate;
    }

    public void updateSubscriptionInfo(Purchase purchase, IabResult result) {
        KeyValue subtype = null;
        KeyValue sub_expiry = null;
        KeyValue iab_response = null;
        KeyValue iab_message = null;
        KeyValue purchase_info = null;
        List<KeyValue> list = KeyValue.listAll(KeyValue.class);
        for (KeyValue keyvalue : list) {
            if (keyvalue.getKey().equals(KeyValue.KEY_SUBS_TYPE))
                subtype = keyvalue;
            else if (keyvalue.getKey().equals(KeyValue.KEY_SUBS_EXPIRY))
                sub_expiry = keyvalue;
            else if (keyvalue.getKey().equals(KeyValue.KEY_IAB_RESULT_RESPONSE))
                iab_response = keyvalue;
            else if (keyvalue.getKey().equals(KeyValue.KEY_IAB_RESULT_MESSAGE))
                iab_message = keyvalue;
            else if (keyvalue.getKey().equals(KeyValue.KEY_PURCHASE_INFO))
                purchase_info = keyvalue;
        }

        if (subtype == null)
            subtype = new KeyValue();
        if (sub_expiry == null)
            sub_expiry = new KeyValue();
        if (iab_response == null)
            iab_response = new KeyValue();
        if (iab_message == null)
            iab_message = new KeyValue();
        if (purchase_info == null)
            purchase_info = new KeyValue();

        Date purchaseTime = new Date();
        purchaseTime.setTime(purchase.getPurchaseTime());
        String date = KeyValue.DateFormat.format(purchaseTime);

        String subs_type = KeyValue.INAPP_ITEM_SUBS_FREE_USER;
        if (purchase.getPurchaseState() == 0)
            subs_type = purchase.getSku();

        Calendar expiry = Calendar.getInstance(Locale.getDefault());
        expiry.setTime(purchaseTime);
        int duration_month = 0;
        if (subs_type.equals(KeyValue.INAPP_ITEM_SUBS_MONTH_1))
            duration_month = 1;
        else if (subs_type.equals(KeyValue.INAPP_ITEM_SUBS_YEAR_1))
            duration_month = 12;

        expiry.add(Calendar.MONTH, duration_month);
        String expiryDate = KeyValue.DateFormat.format(expiry.getTime());

        subtype.initWith(KeyValue.KEY_SUBS_TYPE, subs_type, date);
        sub_expiry.initWith(KeyValue.KEY_SUBS_EXPIRY, expiryDate, date);
        iab_response.initWith(KeyValue.KEY_IAB_RESULT_RESPONSE, String.valueOf(result.getResponse()), date);
        iab_message.initWith(KeyValue.KEY_IAB_RESULT_MESSAGE, result.getMessage(), date);
        purchase_info.initWith(KeyValue.KEY_PURCHASE_INFO, purchase.toString(), date);

        ArrayList<KeyValue> save = new ArrayList<KeyValue>();
        save.add(subtype);
        save.add(sub_expiry);
        save.add(iab_response);
        save.add(iab_message);
        save.add(purchase_info);

        KeyValue.saveInTx(save);
    }

    public boolean isSubscriber() {
        boolean isSubscriber = false;
        try {
            List<KeyValue> list = KeyValue.find(KeyValue.class, "key = ?", KeyValue.KEY_SUBS_TYPE);
            KeyValue keyvalue = null;
            if (list.size() > 0) {
                keyvalue = list.get(0);
                if (keyvalue.getValue().equals(KeyValue.INAPP_ITEM_SUBS_FREE_USER))
                    return isSubscriber;

                try {
                    int duration_month = 1;
                    if (keyvalue.getValue().equals(KeyValue.INAPP_ITEM_SUBS_YEAR_1))
                        duration_month = 12;

                    Date date = KeyValue.DateFormat.parse(keyvalue.getUpdateDate());
                    Calendar updateDate = Calendar.getInstance(Locale.getDefault());
                    updateDate.setTime(date);
                    resetCalendarHourBelow(updateDate);

                    updateDate.add(Calendar.MONTH, duration_month);

                    Calendar now = Calendar.getInstance(Locale.getDefault());
                    resetCalendarHourBelow(now);

                    isSubscriber = updateDate.after(now);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLiteException ex) {
            ex.printStackTrace();
        }
        return isSubscriber;
    }

    public boolean hasSubscriptionRecord() {
        try {
            List<KeyValue> list = KeyValue.find(KeyValue.class, "key = ?", KeyValue.KEY_SUBS_TYPE);
            return list.size() > 0;
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return false;
    }

    class GetVersionAsyncTask extends AsyncTask<Void, Void, VersionInfo> {

        @Override
        protected VersionInfo doInBackground(Void... params) {
            try {
                VersionInfo versionInfo = myApiService.getVersion().execute();

                return versionInfo;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(VersionInfo versionInfo) {
            if (versionInfo == null) {
                Log.e(TAG, "versionInfo is null, check network.");
                return;
            }

            Log.i(TAG, "versionCode: " + versionInfo.getVersionCode());
            Log.i(TAG, "productVersion: " + versionInfo.getProductVersion());

            for (Database.DataBaseListener listener : listeners) {
                listener.onVersionInfoReceived(versionInfo);
            }
        }
    }

    class LoadProductAsyncTask extends AsyncTask<Void, Void, List<Product>> {

        @Override
        protected List<Product> doInBackground(Void... params) {
            long startTime = System.currentTimeMillis();
            List<Product> products = Product.listAll(Product.class);
            long listAllTime = System.currentTimeMillis();
            for (Product product : products) {
                product.makeComparisonData();
            }
            long endTime = System.currentTimeMillis();
            Log.d(TAG, "[TIMESPENT] Product.listAll(Product.class) ==> " + (listAllTime-startTime) + " ms" );
            Log.d(TAG, "[TIMESPENT] LOOP product.makeComparisonData ==> " + (endTime-listAllTime) + " ms" );
            return products;
        }

        @Override
        protected void onPostExecute(List<Product> products) {
            for (Database.DataBaseListener listener : listeners) {
                boolean clearCache = false;
                listener.onProductListCreated(products, clearCache);
            }
        }
    }

    class EndpointsAsyncTask extends AsyncTask<Pair<Context, String>, Void, List<Product>> {

        private Context context;

        @Override
        protected List<Product> doInBackground(Pair<Context, String>... params) {
            context = params[0].first;
            String name = params[0].second;

            try {
//                ProductList resp = myApiService.getProductList().execute();
                ProductList resp = myApiService.getProductListFromJSON().execute();
                byte[] stream = resp.decodeBytes();
                String response = new String(stream, Charset.forName("UTF-8"));
                if (response == null) {
                    // [2016.10.21] 네트워크 이유로 실패한다면 한 번만 더 해본다
                    resp = myApiService.getProductListFromJSON().execute();
                    stream = resp.decodeBytes();
                    response = new String(stream, Charset.forName("UTF-8"));
                    if (response == null) {
                        return new ArrayList<Product>();
                    }
                }

                List<Product> products = getProductList(context, response);
                if (products.size() > 0) {
                    Product.deleteAll(Product.class);
                    Product.saveInTx(products);
                    recordUpdateDate(products.size());
                }
                return products;
            } catch (IOException e) {
                e.printStackTrace();
                return new ArrayList<Product>();
            }
        }

        @Override
        protected void onPostExecute(List<Product> products) {
            for (Database.DataBaseListener listener : listeners) {
                boolean clearCache = true;
                listener.onProductListCreated(products, clearCache);
            }

            if (products.size() > 0) {
                Toast.makeText(context, R.string.finish_update_new_products, Toast.LENGTH_LONG).show();

                SharedPreferences pref = context.getSharedPreferences(PREF_NAME, 0);
                int versionCode = pref.getInt(PREF_FORCE_TO_UPDATE_VERSION_CODE, 0);
                if (versionCode == 0 || versionCode < forceToUpdateVersionCode) {
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt(PREF_FORCE_TO_UPDATE_VERSION_CODE, forceToUpdateVersionCode);
                    editor.commit();
                }
            }
            else {
                Toast.makeText(context, R.string.fail_update_new_products, Toast.LENGTH_LONG).show();
            }

//            String message = String.format("%d", products.size());
//            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

}
