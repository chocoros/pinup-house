package com.dag.app.products;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Product extends SugarRecord<Product> {
    // official information
    private String name;
    private int vps;
    private int pv;
    private int bv;
    private int price;
    private String sku;
    private String brand;
    private String category1;   // subbrand
    private String category2;   // promotion
    private String category3;   // imagesrc
    private String alias;

    // custom information
    @Ignore
    private String compareName;
    @Ignore
    private String compareBrand;
    @Ignore
    private String compareSubbrand;
    @Ignore
    private String compareAlias;
    @Ignore
    private ArrayList<String> tags;

    // for cart
    @Ignore
    private int count;

    public Product() {
    }

    public void initWith(String name, String vps, String pv, String bv, String price, String sku, String brand, String subbrand, String promotion, String imagesrc, String alias) {
        initWith(name, Integer.valueOf(vps), Integer.valueOf(pv), Integer.valueOf(bv), Integer.valueOf(price), sku, brand, subbrand, promotion, imagesrc, alias);
    }

    public void initWith(String name, int vps, int pv, int bv, int price, String sku, String brand, String subbrand, String promotion, String imagesrc, String alias) {
        this.name = name;
        this.vps = vps;
        this.pv = pv;
        this.bv = bv;
        this.price = price;
        this.sku = sku;
        this.brand = brand;
        this.category1 = subbrand;
        this.category2 = promotion;
        this.category3 = imagesrc;
        this.alias = alias;

        this.tags = new ArrayList<String>();
        if (tags != null)
            this.tags.addAll(tags);

        this.count = 0;

        makeComparisonData();
    }

    public String getName() {
        return name;
    }

    public int getVPS() {
        return vps;
    }

    public int getPV() {
        return pv;
    }

    public int getBV() {
        return bv;
    }

    public int getPrice() {
        return price;
    }

    public String getSKU() {
        return sku;
    }

    public String getBrand() { return brand; }
    public String getSubbrand() { return category1; }
    public String getPromotion() { return category2; }
    public String getImageSrc() { return category3; }
    public String getAlias() {
        return alias;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void makeComparisonData() {
        this.compareName = name.toLowerCase(Locale.getDefault()).replaceAll("\\s", "");
        this.compareBrand = brand == null ? "" : brand.toLowerCase(Locale.getDefault()).replaceAll("\\s", "");
        this.compareSubbrand = category1 == null ? "" : category1.toLowerCase(Locale.getDefault()).replaceAll("\\s", "");
        this.compareAlias = alias == null ? "" : alias.toLowerCase(Locale.getDefault()).replaceAll("\\s", "");

        //Log.d("Product", "[name] " + compareName + ", " + compareBrand + ", " + compareSubbrand + ", " + category2 + ", " + category3);
        //Log.d("Product", "[alias] " + compareAlias);
    }

    public boolean filtered(String keyword) {
        if (this.name == null)
            return false;

        if (this.compareName.contains(keyword))
            return true;
        if (this.compareAlias.contains(keyword))
            return true;
        if (this.compareBrand.contains(keyword))
            return true;
        if (this.compareSubbrand.contains(keyword))
            return true;

        return false;
    }
	
	// 가격 검색 메소드, 이상/이하 조건이며, -1이 들어올 경우 보다 높거나 낮은 가격을 검색하고 그 외의 경우는 사이값을 검색한다.
	private boolean containsNumber(int low, int high, int number) {
		if (low == -1 && high == -1)
			return false;

		// high 보다 낮은 가격들 검색 조건
		if (low == -1) {
			return number <= high;
		}
		// low 보다 높은 가격들 검색 조건
		else if (high == -1) {
			return low <= number;
		}
		// low~high 사이의 가격들 검색 조건
		else {
			return low <= number && number <= high;
		}
	}

	public boolean filteredWithPrice(int low, int high) {
		return containsNumber(low, high, price);
	}

	public boolean filteredWithPV(int low, int high) {
		return containsNumber(low, high, pv);
	}

}