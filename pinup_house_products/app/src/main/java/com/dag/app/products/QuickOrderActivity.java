package com.dag.app.products;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.regex.Matcher;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionItemTarget;
import com.github.amlcurran.showcaseview.targets.ActionViewTarget;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

public class QuickOrderActivity extends Activity {
    static final String TAG = "QuickOrderActivity";

    private final String URL_QuickOrder = "mobile.abnkorea.co.kr/shop/cart";

    private WebView mWebView;
    private String mCustomJavascript = "";
    private String json = "";

    private final String PREF_NAME = "pref";
    private final String PROP_FIRST_LAUNCH = "PROP_FIRST_LAUNCH_QuickOrderActivity";

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_order);

        getActionBar().setTitle(R.string.action_quick_order);
        init();

        Intent intent = getIntent();
        json = intent.getStringExtra("json");
    }

     @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return;
        }
        super.onBackPressed();
    }

    private void init() {
        mWebView = (WebView)findViewById(R.id.webView);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d(TAG, "[shouldOverrideUrlLoading] " + url);
                if (url.startsWith("http")) {
                    view.loadUrl(url);
                    return true;
                }

                if (startActivityForUrl(url))
                    return true;

                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d(TAG, "[onPageFinished] " + url);
                if (url.endsWith(URL_QuickOrder)) {
//                    displayShowcaseView(0);
                }
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
                result.confirm();
                return true;
            }

        });

        mCustomJavascript = getJSFile("quickorder.js");
        mWebView.loadUrl("https://" + URL_QuickOrder);
    }

    private void addProductList() {
        StringBuilder builder = new StringBuilder();
        String javascript = "javascript:" + mCustomJavascript + "\n";
        mWebView.loadUrl(javascript);

        try {
            JSONArray array = new JSONArray(json);
            String function = "javascript:addProducts(" + array + ");\n";
            mWebView.loadUrl(function);

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showGuideToast(R.string.message_guide_wait_quickorder);
                }
            }, 1000);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getJSFile(String path) {
        StringBuilder JSContent = new StringBuilder();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open(path)));
            String line = "";
            while ((line = reader.readLine()) != null) {
                JSContent.append(line);
                JSContent.append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return JSContent.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quickorder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.action_add_products) {
            addProductList();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showGuideToast(int res) {
        Toast toast = Toast.makeText(this, res, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void displayShowcaseView(int index) {
        if (index == 0) {
            SharedPreferences pref = getSharedPreferences(PREF_NAME, 0);
            boolean isFirstLaunch = pref.getBoolean(PROP_FIRST_LAUNCH, true);
            if (!isFirstLaunch) {
                showGuideToast(R.string.message_guide_click_quickorder);
                return;
            }

            Target target = new ActionItemTarget(this, R.id.action_add_products);
            new ShowcaseView.Builder(this)
                    .setTarget(target)
                    .setContentTitle(R.string.showcaseview_title_smart_quickorder)
                    .setContentText(R.string.showcaseview_content_smart_quickorder)
                    .hideOnTouchOutside()
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {
                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView) {

                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                            showcaseView.setVisibility(View.GONE);
                            displayShowcaseView(1);
                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView) {
                            mWebView.setVisibility(View.INVISIBLE);
                        }
                    })
                    .build();
        }
        else  if (index == 1) {
            Target target = new ActionViewTarget(this, ActionViewTarget.Type.HOME);
            new ShowcaseView.Builder(this)
                    .setTarget(target)
                    .setContentTitle(R.string.showcaseview_title_smart_quickorder_exit)
                    .hideOnTouchOutside()
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {
                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView) {

                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                            showcaseView.setVisibility(View.GONE);

                            SharedPreferences pref = getSharedPreferences(PREF_NAME, 0);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putBoolean(PROP_FIRST_LAUNCH, false);
                            editor.commit();

                            mWebView.setVisibility(View.VISIBLE);
                            showGuideToast(R.string.message_guide_click_quickorder);
                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView) {

                        }
                    })
                    .build();
        }
    }

    private boolean startActivityForUrl(String url) {
        Intent intent;
        // perform generic parsing of the URI to turn it into an Intent.
        try {
            intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
        } catch (URISyntaxException ex) {
            Log.w("Browser", "Bad URI " + url + ": " + ex.getMessage());
            return false;
        }

        // check whether the intent can be resolved. If not, we will see
        // whether we can download it from the Market.
        if (getPackageManager().resolveActivity(intent, 0) == null) {
            String packagename = intent.getPackage();
            if (packagename != null) {
                intent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://search?q=pname:" + packagename));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                try {
                    startActivity(intent);
                    return true;
                } catch (ActivityNotFoundException e) {
                    Log.w("Browser", "No activity found to handle " + url);
                    return false;
                }
            } else {
                return false;
            }
        }

        // sanitize the Intent, ensuring web pages can not bypass browser
        // security (only access to BROWSABLE activities).
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setComponent(null);
        Intent selector = intent.getSelector();
        if (selector != null) {
            selector.addCategory(Intent.CATEGORY_BROWSABLE);
            selector.setComponent(null);
        }

        // Make sure webkit can handle it internally before checking for specialized
        // handlers. If webkit can't handle it internally, we need to call
        // startActivityIfNeeded
        try {
//            intent.putExtra(BrowserActivity.EXTRA_DISABLE_URL_OVERRIDE, true);
            if (startActivityIfNeeded(intent, -1)) {
                return true;
            }
        } catch (ActivityNotFoundException ex) {
            // ignore the error. If no application can handle the URL,
            // eg about:blank, assume the browser can handle it.
        }

        return false;
    }
}
