package com.dag.app.products;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;

import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SearchView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.vending.billing.IInAppBillingService;
import com.dag.app.losmap.LosMapActivity;
import com.dag.app.products.util.IabHelper;
import com.dag.app.products.util.IabResult;
import com.dag.app.products.util.Inventory;
import com.dag.app.products.util.Purchase;
import com.dag.web.products.myApi.model.VersionInfo;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionItemTarget;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.plus.Plus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends FragmentActivity implements DriveBackup.OnDriveBackupListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "MainActivity";
    private final String base64EncodiedPushedkey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA8wnHH+7i0I+d3nHthrzudDwy1JEAnIxEtV4asetoHu2ZkWTkowX4uZc7QHqhwPnSvzvYZmBCorCU2Uz8wNqIacaVYN2hDMvD/EMukGYUfWNv3cR7mLgC8dNxrRo7KiHnpIFd9+hGpzIrrq2DzeVTcfe1II/0988dDttAB7Dr/rnhoGsLIDEswwjDWdFb3gSYdDQFo5VlZ+PWGeV/PCl74SNBB3EmotJ2R/2eWQhGe2cCldwqmyv1xZ8UHcOirQvoUhR6ttuTVr67kTcmMELMpXJRJiraeSQpUbOYEzpNQkC6cKM4AK0qlhKK1V+YfbD4PiHiyDjEf6HELjlmNocL9QIDAQAB";
    private final String PREF_NAME = "pref";
    private final String PROP_FIRST_LAUNCH = "PROP_FIRST_LAUNCH_MainActivity";
    private final String PREF_PRODUCT_VERSION_CODE = "productVersionCode";

    private final int REQUEST_BUYITEM = 1001;

    private String username;
    private MainFragmentAdapter adapter;
    private DecimalFormat decimalFormat;
    private ProgressDialog progressDialog;

    private GoogleApiClient googleApiClient;
    private DriveBackup driveBackup;
    private boolean launchBackup = false;
    private boolean isSaveDBFileToDrive = false;
    private boolean isRestoreDBFileToDrive = false;

    private long backKeyPressedTime = 0;

    private boolean isSubscriber = false;
    private boolean requestUpdateProduct = false;
    private VersionInfo versionInfo = null;

    private IInAppBillingService mService;
    private IabHelper mHelper;
    private ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };
    private IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {
            if (result.isFailure()) {
                Log.d("IAB", "Failed to query inventory: " + result);
                SendConsumeResult(null, result);
                return;
            }

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            List<String> inappList = inventory
                    .getAllOwnedSkus(IabHelper.ITEM_TYPE_SUBS);
            // inappList.add("item01");

            for (String inappSku : inappList) {
                Purchase purchase = inventory.getPurchase(inappSku);
                Log.d("IAB", "Consumeing ... " + inappSku);
                if (!mHelper.isAsyncInProgress())
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            }

            Log.d("IAB", "Query inventory was successful.");
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d("IAB", "Purchase finished: " + result + ", purchase: "
                    + purchase);  //결제 완료 되었을때 각종 결제 정보들을 볼 수 있습니다. 이정보들을 서버에 저장해 놓아야 결제 취소시 변경된 정보를 관리 할 수 있을것 같습니다~

            if (purchase != null) {
                if (!verifyDeveloperPayload(purchase)) {
                    Log.d("IAB",
                            "Error purchasing. Authenticity verification failed.");
                }

                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } else {
                //Toast.makeText(MainActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    // Called when consumption is complete
    private IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d("IAB", "Consumption finished. Purchase: " + purchase
                    + ", result: " + result);
            SendConsumeResult(purchase, result);
        }
    };

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initActionBar();
        initViewPager();
        initData();
        setEventListener();
        initIAB(base64EncodiedPushedkey, true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mServiceConn != null) {
            unbindService(mServiceConn);
        }
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
            backKeyPressedTime = System.currentTimeMillis();
            Toast.makeText(this, R.string.message_will_exit, Toast.LENGTH_SHORT).show();
            return;
        }
        if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
            finish();
        }
    }

    private void initIAB(String strPublicKey, boolean bDebug) {
        Log.d("IAB", "Creating IAB helper." + bDebug);
        mHelper = new IabHelper(this, strPublicKey);

        if (bDebug == true) {
            mHelper.enableDebugLogging(true, "IAB");
        }

        // explicit intent for 5.0
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");

        List<ResolveInfo> resolveInfos = getPackageManager().queryIntentServices(intent, 0);
        if (resolveInfos == null || resolveInfos.size() != 1)
            return;

        ResolveInfo serviceInfo = resolveInfos.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        intent.setComponent(component);

        bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    private void startIAB() {
        if (mHelper.isSetupDone())
            return;

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

            @Override
            public void onIabSetupFinished(IabResult result) {
                // TODO Auto-generated method stub
                boolean bInit = result.isSuccess();
                Log.d("IAB", "IAB Init " + bInit + result.getMessage());

                if (bInit == true) {
                    Log.d("IAB", "Querying inventory.");

                    mHelper.queryInventoryAsync(mGotInventoryListener);
                }
//                Toast.makeText(MainActivity.this, "InAppInit_U",
//                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void InAppBuyItem(final String strItemId) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub

                /*
                 * TODO: for security, generate your payload here for
                 * verification. See the comments on verifyDeveloperPayload()
                 * for more info. Since this is a SAMPLE, we just use an empty
                 * string, but on a production app you should carefully generate
                 * this.
                 */
                String payload = "user_id";

                mHelper.launchPurchaseFlow(MainActivity.this, strItemId,
                        REQUEST_BUYITEM, mPurchaseFinishedListener, payload);

                Log.d("IAB", "InAppBuyItem_U " + strItemId);
            }
        });
    }

    private boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct.
         * It will be the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase
         * and verifying it here might seem like a good approach, but this will
         * fail in the case where the user purchases an item on one device and
         * then uses your app on a different device, because on the other device
         * you will not have access to the random string you originally
         * generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different
         * between them, so that one user's purchase can't be replayed to
         * another user.
         *
         * 2. The payload must be such that you can verify it even when the app
         * wasn't the one who initiated the purchase flow (so that items
         * purchased by the user on one device work on other devices owned by
         * the user).
         *
         * Using your own server to store and verify developer payloads across
         * app installations is recommended.
         */

        return true;
    }

    protected void SendConsumeResult(Purchase purchase, IabResult result) {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("Result", result.getResponse());
            if (purchase != null) {
                jsonObj.put("OrderId", purchase.getOrderId());
                jsonObj.put("Sku", purchase.getSku());
                jsonObj.put("purchaseData", purchase.getOriginalJson());
                jsonObj.put("signature", purchase.getSignature());

                Log.d("IAB", "OrderId"  + purchase.getOrderId());
                Log.d("IAB", "Sku"  + purchase.getSku());
                Log.d("IAB", "purchaseData"  + purchase.getOriginalJson());
                Log.d("IAB", "signature"  + purchase.getSignature());

                Log.d("IAB", purchase.toString());
                Database.getInstance().updateSubscriptionInfo(purchase, result);
                isSubscriber = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        backupDBFileToSDCard();
    }

    private void backupDBFileToSDCard() {
        if (Database.getInstance().existDBFileInPackage(this))
            Database.getInstance().copyDBFileToSDCard(this);
    }
    private void displayShowcaseView(int index) {
        try {
            if (index == 0) {
                SharedPreferences pref = getSharedPreferences(PREF_NAME, 0);
                boolean isFirstLaunch = pref.getBoolean(PROP_FIRST_LAUNCH, true);
                if (!isFirstLaunch)
                    return;

                Target target = new ActionItemTarget(this, R.id.action_search);
                new ShowcaseView.Builder(this)
                        .setTarget(target)
                        .setContentTitle(R.string.showcaseview_title_searchview)
                        .setContentText(R.string.showcaseview_content_searchview)
                        .hideOnTouchOutside()
                        .setStyle(R.style.CustomShowcaseTheme)
                        .setShowcaseEventListener(new OnShowcaseEventListener() {
                            @Override
                            public void onShowcaseViewHide(ShowcaseView showcaseView) {

                            }

                            @Override
                            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                                showcaseView.setVisibility(View.GONE);
                                displayShowcaseView(1);
                            }

                            @Override
                            public void onShowcaseViewShow(ShowcaseView showcaseView) {
                                adapter.setShowcaseViewMode(true);
                            }
                        })
                        .build();
            } else if (index == 1) {
                if (adapter.getProductCount() == 0)
                    return;

                Target target = new ViewTarget(R.id.linearlayout_product_plus_minus, this);
                new ShowcaseView.Builder(this)
                        .setTarget(target)
                        .setContentTitle(R.string.showcaseview_title_plusminus)
                        .setContentText(R.string.showcaseview_content_plusminus)
                        .hideOnTouchOutside()
                        .setStyle(R.style.CustomShowcaseTheme)
                        .setShowcaseEventListener(new OnShowcaseEventListener() {
                            @Override
                            public void onShowcaseViewHide(ShowcaseView showcaseView) {

                            }

                            @Override
                            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                                showcaseView.setVisibility(View.GONE);
                                displayShowcaseView(2);
                            }

                            @Override
                            public void onShowcaseViewShow(ShowcaseView showcaseView) {

                            }
                        })
                        .build();
            } else if (index == 2) {
                Target target = new ViewTarget(R.id.linearylayout_product_total, this);
                new ShowcaseView.Builder(this)
                        .setTarget(target)
                        .setContentTitle(R.string.showcaseview_title_pvbv)
                        .setContentText(R.string.showcaseview_content_pvbv)
                        .hideOnTouchOutside()
                        .setStyle(R.style.CustomShowcaseTheme)
                        .setShowcaseEventListener(new OnShowcaseEventListener() {
                            @Override
                            public void onShowcaseViewHide(ShowcaseView showcaseView) {

                            }

                            @Override
                            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                                showcaseView.setVisibility(View.GONE);
                                displayShowcaseView(3);
                            }

                            @Override
                            public void onShowcaseViewShow(ShowcaseView showcaseView) {

                            }
                        })
                        .build();
            } else if (index == 3) {
                Target target = new ActionItemTarget(this, R.id.action_share);
                new ShowcaseView.Builder(this)
                        .setTarget(target)
                        .setContentTitle(R.string.showcaseview_title_main_menu)
                        .setContentText(R.string.showcaseview_content_main_menu)
                        .hideOnTouchOutside()
                        .setStyle(R.style.CustomShowcaseTheme)
                        .setShowcaseEventListener(new OnShowcaseEventListener() {
                            @Override
                            public void onShowcaseViewHide(ShowcaseView showcaseView) {

                            }

                            @Override
                            public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                                showcaseView.setVisibility(View.GONE);

                                SharedPreferences pref = getSharedPreferences(PREF_NAME, 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putBoolean(PROP_FIRST_LAUNCH, false);
                                editor.commit();

                                adapter.setShowcaseViewMode(false);
                            }

                            @Override
                            public void onShowcaseViewShow(ShowcaseView showcaseView) {

                            }
                        })
                        .build();
            }
        }
        catch (Exception e) {
            // 갤럭시노트1, showcase 첫 진입시 죽는 문제가 있으므로 문제가 생기면 그냥 지나가도록 처리
            SharedPreferences pref = getSharedPreferences(PREF_NAME, 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(PROP_FIRST_LAUNCH, false);
            editor.commit();

            adapter.setShowcaseViewMode(false);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        invalidateOptionsMenu();
    }

    private void setSearchViewListener(final MenuItem searchItem) {
        SearchView searchView = (SearchView) searchItem.getActionView();
        int searchSrcTextId = getResources().getIdentifier("android:id/search_src_text", null, null);
        final EditText searchEditText = (EditText) searchView.findViewById(searchSrcTextId);
        if (searchEditText != null) {
            searchEditText.setTextColor(Color.WHITE);
            searchEditText.setImeOptions(EditorInfo.IME_FLAG_NO_FULLSCREEN);
            searchEditText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        }

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
        searchView.setSearchableInfo(info);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String keyword) {
                searchItem.collapseActionView();
//                adapter.filter(keyword);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String keyword) {
                adapter.filter(keyword, viewPager.getCurrentItem());
                return false;
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // SearchView
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        setSearchViewListener(searchItem);

        MenuItem action_share = menu.findItem(R.id.action_share);
        MenuItem action_quick_order = menu.findItem(R.id.action_quick_order);
        MenuItem action_save = menu.findItem(R.id.action_save);
        MenuItem action_refresh = menu.findItem(R.id.action_refresh);
        MenuItem action_expand_losmap = menu.findItem(R.id.action_expand_losmap);
        MenuItem action_backup = menu.findItem(R.id.action_backup);
//        MenuItem action_subscription = menu.findItem(R.id.action_subscription);
        MenuItem action_expand_all = menu.findItem(R.id.action_expand_all);
        MenuItem action_collapse_all = menu.findItem(R.id.action_collapse_all);

        boolean enable = true;
        if (viewPager.getCurrentItem() == MainFragmentAdapter.CUSTOMER_FRAGMENT) {
            enable = false;
        }

        action_share.setVisible(enable);
        action_quick_order.setVisible(enable);
        action_save.setVisible(enable);
        action_expand_losmap.setVisible(enable);
        action_backup.setVisible(enable);
        action_refresh.setVisible(enable);
//        action_subscription.setVisible(enable);
        action_expand_all.setVisible(!enable);
        action_collapse_all.setVisible(!enable);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        if (id == R.id.action_share) {
            shareSelectedProducts();
            return true;
        }

        if (id == R.id.action_save) {
            saveSelectedProducts();
            return true;
        }

//        if (id == R.id.action_collection) {
//            showSaveCollection();
//            return true;
//        }

        if (id == R.id.action_quick_order) {
            moveToQuickOrderActivity();
            return true;
        }

        if (id == R.id.action_refresh) {
            getNewItems();
            return true;
        }

        if (id == R.id.action_backup) {
            backupDBFile();
            return true;
        }

//        if (id == R.id.action_subscription) {
//            subscribe();
//            return true;
//        }

        if (id == R.id.action_expand_losmap) {
            moveToLosmapActivity();
            return true;
        }

        if (id == R.id.action_expand_all) {
            expandAllCustomer(true);
            return true;
        }

        if (id == R.id.action_collapse_all) {
            expandAllCustomer(false);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    private void expandAllCustomer(boolean expand) {
        adapter.expandAllCustomer(expand);
    }

    private void gotoActivity(Class<LosMapActivity> activityClass) {
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }


    private void subscribe() {
        if (!mHelper.isSetupDone())
            startIAB();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View view = getLayoutInflater().inflate(R.layout.dialog_purchase_subscribe, null);
        final RadioGroup radioGroup = (RadioGroup)view.findViewById(R.id.radiogroup_purchase);
        final RadioButton month_1 = (RadioButton)radioGroup.findViewById(R.id.radiobutton_item_month_1);
        final RadioButton year_1 = (RadioButton)radioGroup.findViewById(R.id.radiobutton_item_year_1);
        month_1.setTag(KeyValue.INAPP_ITEM_SUBS_MONTH_1);
        year_1.setTag(KeyValue.INAPP_ITEM_SUBS_YEAR_1);

        final AlertDialog dialog = builder.setView(view).
                setTitle(R.string.title_subscribe)
                .setNegativeButton(loadString(R.string.button_cancel), null)
                .setPositiveButton(loadString(R.string.button_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create();

        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(MainActivity.this, R.string.message_select_purchase_item, Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.dismiss();
                RadioButton selected = (RadioButton)radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
                InAppBuyItem((String) selected.getTag()); // 제품id를 써줍니다. 앱배포시에 인앱상품등록시  등록할 id입니다.
            }
        });
    }

    private void getNewItems() {
        if (!mHelper.isSetupDone())
            startIAB();

        isSubscriber = Database.getInstance().isSubscriber();

        // donglove0324 and mychocoros are managers.
        // [chocoros] unlimited version
        //if (username != null && username.equalsIgnoreCase("donglove0324@gmail.com") || username.equalsIgnoreCase("mychocoros@gmail.com"))
            isSubscriber = true;

        if (isSubscriber || Database.getInstance().isInExpiry() || Database.getInstance().canUpdateProductsWithFreeUser()) {
            updateNewProducts();
        }
        else {
            // 구독 API로 구독 여부 확인 필요.
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setNegativeButton(loadString(R.string.button_ok), null)
                    .setPositiveButton(loadString(R.string.goto_subscribe), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            subscribe();
                        }
                    });

            if (Database.getInstance().hasSubscriptionRecord()) {
                builder.setTitle(R.string.title_expire_subscribe)
                        .setMessage(R.string.dialog_message_expire_subscribe);

                // Purchases.subscriptions 확인
            }
            else {
                builder.setTitle(R.string.update_new_products)
                        .setMessage(R.string.dialog_message_update_products);
            }
            builder.create().show();
        }
    }

    private void updateNewProducts() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(loadString(R.string.updating) + "...");
        progressDialog.show();

        Database.getInstance().updateProductList(this);
    }

//    private Database getDatabase() {
//        return Database.getInstance();
//    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d(TAG, "[onNewIntent] intent=" + intent);

        if (intent != null) {
            boolean defaultValue = false;
            launchBackup = intent.getBooleanExtra(DriveBackup.ACTION_DRIVE_BACKUP, defaultValue);
            if (launchBackup) {
                if (googleApiClient.isConnected())
                    driveBackup.saveDBFileToDrive();
                else {
                    googleApiClient.connect();
                    isSaveDBFileToDrive = true;
                }
            }
        }
    }

    private void initActionBar() {
        //getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setTitle(R.string.app_actionbar_name);
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.main_viewpager);
        adapter = new MainFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setPageMargin(dpToPixel(15));
        viewPager.setPageMarginDrawable(R.color.tab_indicator);
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                invalidateOptionsMenu();
            }
        });

        PagerTabStrip strip = (PagerTabStrip) findViewById(R.id.tab_indicator);
        strip.setDrawFullUnderline(true);
        strip.setTabIndicatorColor(getResources().getColor(R.color.tab_indicator));
        strip.setTextColor(getResources().getColor(android.R.color.white));

        invalidateOptionsMenu();
    }

    public int dpToPixel(int dp){
        int px = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (int)dp, getResources().getDisplayMetrics());
        return px;
    }

    private void initData() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);

        // Create the API client and bind it to an instance variable.
        // We use this instance as the callback for connection and connection
        // failures.
        // Since no account name is passed, the user is prompted to choose.
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addApi(Plus.API)
                .addScope(Drive.SCOPE_FILE)
                .addScope(Drive.SCOPE_APPFOLDER)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .useDefaultAccount()
                .build();


        driveBackup = new DriveBackup(googleApiClient, this, this);
        //setAlarmForBackup();

        Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
        if (accounts.length > 0) {
            username = accounts[0].name;
        }

        for (Account account : accounts) {
            Log.d(TAG, "[DATABASE] account.name=" + account.name);
        }
    }

    private boolean updateProductList(VersionInfo info)
    {
        SharedPreferences pref = getSharedPreferences(PREF_NAME, 0);
        int productVersionCode = pref.getInt(PREF_PRODUCT_VERSION_CODE, 0);
        if (productVersionCode == 0 || productVersionCode < info.getProductVersion()) {
            updateNewProducts();

            versionInfo = info;
            requestUpdateProduct = true;
            return true;
        }
        return false;
    }

    private void updateApk(VersionInfo info)
    {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            if (packageInfo.versionCode < info.getVersionCode()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setMessage(R.string.message_update_apk)
                        .setCancelable(true)
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                new AsyncTask<Void, Void, Void>() {
                                    @Override
                                    protected void onPreExecute() {
                                        progressDialog = new ProgressDialog(MainActivity.this);
                                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                        progressDialog.setIndeterminate(true);
                                        progressDialog.setMessage(loadString(R.string.downloading) + "...");
                                        progressDialog.show();
                                    }

                                    @Override
                                    protected Void doInBackground(Void... params) {
                                        // 웹서버에 apk 파일을 다운로드하고 설치하자.
                                        String apkurl = "https://pinup-44ea2.firebaseapp.com/apps/android/pinup-house-android.apk";
                                        String rootDirectory = "PinupHouse";

                                        try {
                                            URL url = new URL(apkurl);
                                            URLConnection urlConnection = url.openConnection();
                                            urlConnection.setDoOutput(true);
                                            urlConnection.connect();

                                            File sdcard = Environment.getExternalStorageDirectory();
                                            if (sdcard.canWrite()) {
                                                File dir = new File(sdcard, rootDirectory);
                                                if (dir.exists() == false)
                                                    dir.mkdir();

                                                File apkfile = new File(dir, "pinup-house-android.apk");
                                                FileOutputStream fout = new FileOutputStream(apkfile);
                                                InputStream fin = urlConnection.getInputStream();
                                                int fileSize = urlConnection.getContentLength();

                                                byte[] bytebuffer = new byte[2048];
                                                int bufferLength = 0;
                                                while ((bufferLength = fin.read(bytebuffer)) > 0) {
                                                    fout.write(bytebuffer, 0, bufferLength);
                                                }
                                                fout.close();

                                                Uri apkUri = Uri.fromFile(apkfile);

                                                Intent installIntent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                                                installIntent.setDataAndType(apkUri, "application/vnd.android.package-archive");

                                                startActivity(installIntent);
                                            }

                                        } catch (MalformedURLException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        } finally {

                                        }
                                        return null;
                                    }

                                    @Override
                                    protected void onPostExecute(Void aVoid) {
                                        if (progressDialog.isShowing())
                                            progressDialog.hide();
                                    }
                                }.execute();

                                // 마켓에 앱이 있을 경우 플레이 스토어로 이동하여 업데이트
                                /*
                                Intent goToMarket = new Intent(Intent.ACTION_VIEW)
                                        .setData(Uri.parse("market://details?id=" + getPackageName()));
                                if (goToMarket != null) {
                                    startActivity(goToMarket);
                                }
                                */
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        } catch (ActivityNotFoundException e){
            Toast.makeText(this, R.string.message_not_found_apk, Toast.LENGTH_SHORT).show();
            e.printStackTrace();

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    private void setEventListener() {
        
        // load list
        Database.getInstance().setOnDataBaseListener(new Database.DataBaseListener() {
            @Override
            public void onProductListCreated(List<Product> list, boolean clearCache) {
                if (progressDialog != null)
                    progressDialog.hide();

                //displayShowcaseView(0);

                isSubscriber = Database.getInstance().isSubscriber();

                // developer console  -> 서비스및 API -> 어플리케이션용 라이센스 키를 복사해서 넣으시면 됩니다.
                if (!isSubscriber) {
                    startIAB();
                }

                // 신제품 업데이트가 성공한 이후에 다시 목록이 업데이트 되었을 때 버전을 기록한다.
                if (requestUpdateProduct && versionInfo != null) {
                    SharedPreferences pref = getSharedPreferences(PREF_NAME, 0);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt(PREF_PRODUCT_VERSION_CODE, versionInfo.getProductVersion());
                    editor.commit();

                    requestUpdateProduct = false;
                    versionInfo = null;
                }


                Database.getInstance().checkVersion(MainActivity.this);
            }

            @Override
            public void onRequestGoogleApiClientConnection(boolean isSave, boolean isRestore) {
                googleApiClient.connect();
                isSaveDBFileToDrive = isSave;
                isRestoreDBFileToDrive = isRestore;
            }

            @Override
            public void onProgressMessageChanged(int id) {
                if (progressDialog != null)
                    progressDialog.setMessage(loadString(id) + "...");
            }

            @Override
            public void onVersionInfoReceived(VersionInfo info) {
                // 제품 업데이트를 먼저 하고 다음 실행 시 apk를 업데이트 한다.

                // 제품 업데이트는 사용자에게 토스트로 보여주며 자동으로 진행한다.
                if (updateProductList(info))
                    return;

                // apk 업데이트는 사용자에게 팝업으로 물어본다.
                updateApk(info);
            }
        });
    }

    private void setAlarmForBackup() {

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, MainActivity.class);
        boolean launchBackup = true;
        intent.putExtra(DriveBackup.ACTION_DRIVE_BACKUP, launchBackup);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, DriveBackup.REQUEST_CODE_BACKUP_PERIODICALLY, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // get first noon
        Calendar now = Calendar.getInstance(Locale.getDefault());
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);

        int hour = now.get(Calendar.HOUR);
        int addHour = (12 - hour);
        now.add(Calendar.HOUR, addHour);

        long startTime = now.getTimeInMillis();
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, startTime, AlarmManager.INTERVAL_HALF_DAY, pendingIntent);
    }

     private String getDecimalFormat(int number) {
        if (decimalFormat == null)
            decimalFormat = new DecimalFormat("###,###,###");
        return decimalFormat.format(number);
    }

    private String loadString(int id) {
        return getString(id);
    }

    private void shareSelectedProducts() {
        final List<Product> selectedList = adapter.getSelected();
        if (selectedList.size() == 0) {
            Toast.makeText(this, R.string.message_select_product, Toast.LENGTH_SHORT).show();
            return;
        }

        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_check_share_info, null);
        final CheckBox checkbox_price = (CheckBox) dialogView.findViewById(R.id.checkbox_display_price);
        final CheckBox checkbox_pv = (CheckBox) dialogView.findViewById(R.id.checkbox_display_pv);
        final CheckBox checkbox_cashback = (CheckBox) dialogView.findViewById(R.id.checkbox_display_cashback);
        final ToggleButton toggle_select_all = (ToggleButton) dialogView.findViewById(R.id.toggle_select_all);
        toggle_select_all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkbox_price.setChecked(isChecked);
                checkbox_pv.setChecked(isChecked);
                checkbox_cashback.setChecked(isChecked);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_share_check_info);
        builder.setView(dialogView);
        builder.setNegativeButton(R.string.button_cancel, null);
        builder.setPositiveButton(R.string.action_share, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // make text
                StringBuilder builder = new StringBuilder();
                builder.append(String.format("%s\n", loadString(R.string.share_product_list_title)));

                int pv = 0;
                int bv = 0;
                int price = 0;
                for (Product product : selectedList) {
                    builder.append(String.format("%s : %d%s %s%s\n", product.getName(), product.getCount(), loadString(R.string.unit), getDecimalFormat(product.getPrice() * product.getCount()), loadString(R.string.currency)));

                    pv += product.getPV() * product.getCount();
                    bv += product.getBV() * product.getCount();
                    price += product.getPrice() * product.getCount();
                }

//                String title = String.format("\n%s\n", loadString(R.string.share_total_title));
                String title = "\n";
                String textPrice = String.format("%s: %s%s\n", loadString(R.string.price), getDecimalFormat(price), loadString(R.string.currency));
                String textPV = String.format("%s: %s\n", loadString(R.string.pv), getDecimalFormat(pv));
                String textCashback = String.format("%s: %s%s\n", loadString(R.string.cashback), getDecimalFormat(CashbackCalculator.calculate1stCachback(pv, bv)), loadString(R.string.currency));

                builder.append(title);
                if (checkbox_price.isChecked())
                    builder.append(textPrice);
                if (checkbox_pv.isChecked())
                    builder.append(textPV);
                if (checkbox_cashback.isChecked())
                    builder.append(textCashback);

                String content = content = builder.toString();

                // send
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getText(R.string.action_collection) + " " + getResources().getText(R.string.action_share));
                intent.putExtra(Intent.EXTRA_TEXT, content);
                // [2017.04.20] 카카오톡 공유 인텐트 :: 마시멜로 chooser 고정 문제
                startActivity(Intent.createChooser(intent, getResources().getText(R.string.action_share)));

            }
        });
        builder.show();
    }

    private void saveSelectedProducts() {
        final List<Product> selectedList = adapter.getSelected();
        if (selectedList.size() == 0) {
            Toast.makeText(this, R.string.message_select_product, Toast.LENGTH_SHORT).show();
            return;
        }

        View dialogView = getLayoutInflater().inflate(R.layout.dialog_save_cart, null);
        final AutoCompleteTextView edittext_personName = (AutoCompleteTextView)dialogView.findViewById(R.id.edittext_person_name);
        final EditText edittext_cartTitle = (EditText)dialogView.findViewById(R.id.edittext_cart_title);
        final EditText edittext_customer_memo = (EditText)dialogView.findViewById(R.id.edittext_customer_memo);

        final ArrayAdapter<String> nameAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, adapter.getPersonNameList());
        edittext_personName.setAdapter(nameAdapter);
        edittext_personName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String personName = nameAdapter.getItem(position);
                Customer customer = adapter.getPerson(personName);
                edittext_customer_memo.setText(customer.memo0);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.save_cart);
        builder.setView(dialogView);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
            }
        });
        builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String personName = edittext_personName.getText().toString();
                if (personName == null || personName.length() == 0) {
                    Toast.makeText(MainActivity.this, R.string.message_input_name, Toast.LENGTH_SHORT).show();
                    return;
                }

                final String title = edittext_cartTitle.getText().toString();
                final String memo0 = edittext_customer_memo.getText().toString();

                new AsyncTask<List<Product>, Void, Boolean>() {

                    @Override
                    protected Boolean doInBackground(List<Product>... params) {
                        Date date = new Date();
                        int pv = 0;
                        int bv = 0;
                        int price = 0;
                        StringBuilder brief = new StringBuilder();
                        ArrayList<Pair<Product, Integer>> list = new ArrayList<Pair<Product, Integer>>();
                        for (int i = 0; i < selectedList.size(); i++) {
                            Product product = selectedList.get(i);

                            pv += product.getPV() * product.getCount();
                            bv += product.getBV() * product.getCount();
                            price += product.getPrice() * product.getCount();

                            if (i > 0)
                                brief.append(", ");
                            brief.append(String.format("%s", product.getName()));

                            Pair<Product, Integer> pair = new Pair<Product, Integer>(product, product.getCount());
                            list.add(pair);
                        }

                        Cart cart = new Cart();
                        cart.initWith(personName, title, date, pv, bv, price, brief.toString(), -1);

                        Customer customer = adapter.getPerson(personName);
                        if (customer == null) {
                            customer = adapter.getPerson(cart); // create new Customer
                            customer.memo0 = memo0;
                            customer.save();
                        }
                        
                        cart.setPersonId(customer.getId());
                        cart.save();
                        cart.saveProductList(list);

                        return true;
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        dialog.dismiss();

                        if (aBoolean) {
                            adapter.updateCustomerList();
                            Toast.makeText(MainActivity.this, R.string.save_success, Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(MainActivity.this, R.string.save_failure, Toast.LENGTH_SHORT).show();
                    }
                }.execute(selectedList);
            }
        });
    }

    private void showSaveCollection() {
        viewPager.setCurrentItem(MainFragmentAdapter.CUSTOMER_FRAGMENT);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                adapter.decreaseTextSize();
                return true;
            case KeyEvent.KEYCODE_VOLUME_UP:
                adapter.increaseTextSize();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void moveToQuickOrderActivity() {
        final List<Product> selectedList = adapter.getSelected();
        if (selectedList.size() == 0) {
            Toast.makeText(this, R.string.message_select_product, Toast.LENGTH_SHORT).show();
            return;
        }

        ArrayList<ProductJson> jsonarray = new ArrayList<ProductJson>();
        for (Product product : selectedList) {
            ProductJson json = new ProductJson();
            json.vps = product.getVPS();
            json.count = product.getCount();
            jsonarray.add(json);
        }

        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        java.lang.reflect.Type collectionType = new TypeToken<ArrayList<ProductJson>>(){}.getType();
        String json = gson.toJson(jsonarray, collectionType);
//        json = json.replace('\"', '\'');

        Intent intent = new Intent(this, QuickOrderActivity.class);
        intent.putExtra("json", json);
        startActivity(intent);
    }

    private void moveToLosmapActivity() {
        gotoActivity(LosMapActivity.class);
    }


    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case DriveBackup.REQUEST_CODE_CREATOR:
                // Called after a file is saved to Drive.
                if (resultCode == RESULT_OK) {
                    Log.i(TAG, "Image successfully saved.");
                    progressDialog.dismiss();
                    Toast.makeText(this, R.string.save_success, Toast.LENGTH_SHORT).show();
                }
            break;
            case DriveBackup.REQUEST_CODE_RESOLUTION:
                if (resultCode == RESULT_OK) {
                    googleApiClient.connect();
                }
                break;
            case REQUEST_BUYITEM:
                // Pass on the activity result to the helper for handling
                if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
                    // not handled, so handle it ourselves (here's where you'd
                    // perform any handling of activity results not related to
                    // in-app
                    // billing...
                    super.onActivityResult(requestCode, resultCode, data);
                } else {
                    Log.d("IAB", "onActivityResult handled by IABUtil.");
                }
                break;
        }
    }

    private void backupDBFile() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(loadString(R.string.upload_to_drive) + "...");
        progressDialog.show();

        if (googleApiClient.isConnected())
            driveBackup.saveDBFileToDrive();
        else {
            googleApiClient.connect();
            isSaveDBFileToDrive = true;
        }
    }

    @Override
    public void onDriveBackupResult(boolean result, boolean needReload) {
        // restoreDBFileFromDrive case
        if (needReload) {
            Database.getInstance().loadProductListDirect(this);
            return;
        }

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        if (!result)
            Toast.makeText(MainActivity.this, R.string.upload_to_drive_fail, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(MainActivity.this, R.string.upload_to_drive_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "API client connected.");
        if (isSaveDBFileToDrive) {
            driveBackup.saveDBFileToDrive();
            isSaveDBFileToDrive = false;
        }
        if (isRestoreDBFileToDrive) {
            driveBackup.restoreDBFileFromDrive();
            isRestoreDBFileToDrive = false;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Called whenever the API client fails to connect.
        Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());
        if (!result.hasResolution()) {
            // show the localized error dialog.
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }
        // The failure has a resolution. Resolve it.
        // Called typically when the app is not yet authorized, and an
        // authorization
        // dialog is displayed to the user.
        try {
            result.startResolutionForResult(this, DriveBackup.REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "Exception while starting resolution activity", e);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleApiClient connection suspended");
    }
}
