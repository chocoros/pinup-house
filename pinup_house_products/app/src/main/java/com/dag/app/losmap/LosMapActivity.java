package com.dag.app.losmap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.dag.app.products.R;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionItemTarget;
import com.github.amlcurran.showcaseview.targets.Target;

public class LosMapActivity extends Activity {

    private final String URL_LosMapInfo = "mobile.abnkorea.co.kr/business/losmap/business-results";

    private WebView mWebView;
    private String mCustomJavascript = "";
    private AlertDialog.Builder mUsageDialog;

    private final String PREF_NAME = "pref";
    private final String PROP_FIRST_LAUNCH = "PROP_FIRST_LAUNCH_LosMapActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losmap);

        getActionBar().setTitle(R.string.activity_losmap);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_losmap, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_showall:
            expandAndShowAll();
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return;
        }
        super.onBackPressed();
    }

    private void init() {
        mWebView = (WebView)findViewById(R.id.webView);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (url.toLowerCase().contains(URL_LosMapInfo.toLowerCase())) {
//                    displayShowcaseView(0);
                }
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
                result.confirm();
                return true;
            }

        });

        mCustomJavascript = getJSFile("losmap.js");
        mWebView.loadUrl("https://" + URL_LosMapInfo);

        String formatMessage = "<div>" + "<div>1. LOSMAP을 모두 펼쳐서 ABO 오른쪽에 <b>퍼센트 핀</b>과 <b>PV</b>를 보여 줍니다.</div>"
                               + "<div>2. 모두 펼친 후에 나의 ABO 오른쪽에 <b>전체 회원수</b>를 보여 줍니다.</div>"
                               + "<div>3. 다시 하려면 sABN의 오른쪽 상단의 LOSMAP을 클릭 한 후 다시 모두 펼치기 메뉴를 선택합니다.</div>"
                               + "</div>";
        mUsageDialog = new AlertDialog.Builder(this);
        mUsageDialog.setPositiveButton(android.R.string.ok, null);
        mUsageDialog.setTitle(R.string.action_usage);
        mUsageDialog.setMessage(Html.fromHtml(formatMessage));
    }

    private void expandAndShowAll() {
        String javascript = "javascript:" + mCustomJavascript + "\n";
        mWebView.loadUrl(javascript);

        String function = "javascript:expandAndShowAll();\n";
        mWebView.loadUrl(function);
    }

    private String getJSFile(String path) {
        StringBuilder JSContent = new StringBuilder();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getAssets().open(path)));
            String line = "";
            while ((line = reader.readLine()) != null) {
                JSContent.append(line);
                JSContent.append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return JSContent.toString();
    }

    private void displayShowcaseView(int index) {
        if (index == 0) {
            SharedPreferences pref = getSharedPreferences(PREF_NAME, 0);
            boolean isFirstLaunch = pref.getBoolean(PROP_FIRST_LAUNCH, true);
            if (!isFirstLaunch) {
                return;
            }

            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(PROP_FIRST_LAUNCH, false);
            editor.commit();

            Target target = new ActionItemTarget(this, R.id.action_showall);
            new ShowcaseView.Builder(this)
                    .setTarget(target)
                    .setContentTitle(R.string.showcaseview_title_losmap_expandall)
                    .setContentText(R.string.showcaseview_content_losmap_expandall)
                    .hideOnTouchOutside()
                    .setStyle(R.style.CustomShowcaseTheme)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {
                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView) {

                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                            showcaseView.setVisibility(View.GONE);
                            mWebView.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView) {
                            mWebView.setVisibility(View.INVISIBLE);
                        }
                    })
                    .build();
        }
    }
}
