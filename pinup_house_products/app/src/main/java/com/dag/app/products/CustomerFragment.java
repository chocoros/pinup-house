package com.dag.app.products;

import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.dag.web.products.myApi.model.VersionInfo;

import java.text.DecimalFormat;
import java.util.List;

public class CustomerFragment extends Fragment {

    public interface OnCartClickListener {
        public void onCartClick(Cart cart);
    }

    private DecimalFormat decimalFormat;
    private ExpandableListView expandableListView;
	private SaveExpandableListAdapter saveExpandableListAdapter;
	private TextView customerNumber;
	private TextView monthlyPV;
	private TextView monthlyBV;
    private OnCartClickListener onCartClickListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_manage_customers, container, false);
		setEventListener(view);
        return view;
    }

    private void setEventListener(View parent) {
        customerNumber = (TextView) parent.findViewById(R.id.textview_customer_number);
        monthlyPV = (TextView) parent.findViewById(R.id.textview_monthly_pv);
        monthlyBV = (TextView) parent.findViewById(R.id.textview_monthly_bv);

        expandableListView = (ExpandableListView) parent.findViewById(R.id.expandableListView_savecollection);
        saveExpandableListAdapter = new SaveExpandableListAdapter(getActivity(), parent.findViewById(R.id.framelayout_drawer_container));
        saveExpandableListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onInvalidated() {
                super.onInvalidated();
                updateTitleInfo();
            }

            @Override
            public void onChanged() {
                super.onChanged();
                updateTitleInfo();
            }
        });

        expandableListView.setAdapter(saveExpandableListAdapter);
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Cart cart = (Cart) saveExpandableListAdapter.getChild(groupPosition, childPosition);
                if (onCartClickListener != null)
                    onCartClickListener.onCartClick(cart);
                return true;
            }
        });
        expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                int itemType = ExpandableListView.getPackedPositionType(id);
                int groupPosition = -1;
                int childPosition = -1;
                if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    childPosition = ExpandableListView.getPackedPositionChild(id);
                    groupPosition = ExpandableListView.getPackedPositionGroup(id);

                    saveExpandableListAdapter.onLongClickChild(groupPosition, childPosition);
                    return true;

                } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                    groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    saveExpandableListAdapter.onLongClickGroup(groupPosition);
                    return true;
                }
                // null item; we don't consume the click
                return false;
            }
        });

        // load list
        Database.getInstance().setOnDataBaseListener(new Database.DataBaseListener() {
            @Override
            public void onProductListCreated(List<Product> list, boolean clearCache) {
                saveExpandableListAdapter.loadSaveList();
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        saveExpandableListAdapter.loadProductList();
                        return null;
                    }
                }.execute();
            }

            @Override
            public void onRequestGoogleApiClientConnection(boolean isSave, boolean isRestore) {
            }

            @Override
            public void onProgressMessageChanged(int id) {
            }

            @Override
            public void onVersionInfoReceived(VersionInfo info) {

            }
        });

    }

    private void updateTitleInfo() {
        if (!isAdded())
            return;

		if (customerNumber != null) {
			String text = getString(R.string.text_info_customer_number) + " : " + getDecimalFormat(saveExpandableListAdapter.getCustomerNumber()) + getString(R.string.text_info_unit_person);
			customerNumber.setText(text);
		}
		if (monthlyPV != null) {
			String text = getString(R.string.text_info_monthly_pv) + " : " + getDecimalFormat(saveExpandableListAdapter.getMonthlyPV());
			monthlyPV.setText(text);
		}

		if (monthlyBV != null) {
			String text = getString(R.string.text_info_monthly_bv) + " : " + getDecimalFormat(saveExpandableListAdapter.getMonthlyBV());
			monthlyBV.setText(text);
		}
    }

    private String getDecimalFormat(int number) {
        if (decimalFormat == null)
            decimalFormat = new DecimalFormat("###,###,###");
        return decimalFormat.format(number);
    }

    public void updateCustomerList() {
        saveExpandableListAdapter.loadSaveList();
    }

    public List<String> getPersonNameList() {
        return saveExpandableListAdapter.getPersonNameList();
    }

    public Customer getPerson(Cart cart) {
        return saveExpandableListAdapter.getPerson(cart);
    }

    public Customer getPerson(String personName) { return saveExpandableListAdapter.getPerson(personName); }

    public void filter(String keyword) {
        if (saveExpandableListAdapter != null) {
            int groupPosition = saveExpandableListAdapter.filter(keyword);
            expandAllCustomer(groupPosition >= 0);
        }
    }

    public void expandAllCustomer(boolean expand) {
        for (int i = saveExpandableListAdapter.getGroupCount() -1; i >= 0 ; i--) {
            if (expand) 
                expandableListView.expandGroup(i, true);
            else
                expandableListView.collapseGroup(i);   
        }
    }

    public void setOnCartClickListener(CustomerFragment.OnCartClickListener listener) {
        onCartClickListener = listener;
    }

}