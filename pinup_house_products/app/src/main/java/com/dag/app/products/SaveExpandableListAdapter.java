package com.dag.app.products;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.Collator;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.RuleBasedCollator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SaveExpandableListAdapter extends BaseExpandableListAdapter {

    private static final String TAG = "SaveList";
    private LayoutInflater inflater;
    private ArrayList<Customer> personList;
    private ArrayList<Customer> totalPersonList;
    HashMap<Long, Customer> mapCustomer;
    private DateFormat dateFormat;
    private DecimalFormat decimalFormat;
    private String dateFormatString = "yyyy-MM-dd";
    private ProgressBar progress;

    private long MONTH_1 = (long)(1 * 30 * 24 * 60 * 60) * (long)1000;
    private long MONTH_2 = 2 * MONTH_1;
    private long MONTH_3 = 3 * MONTH_1;

    public SaveExpandableListAdapter(Context context, View parent) {
        this.inflater = LayoutInflater.from(context);
        personList = new ArrayList<Customer>();
        totalPersonList = new ArrayList<Customer>();
        mapCustomer = new HashMap<Long, Customer>();
        dateFormat = new SimpleDateFormat(dateFormatString);

        progress = (ProgressBar) parent.findViewById(R.id.progressbar_cart_list);
    }

    public void loadSaveList() {
        AsyncTask<Void, Void, List<Customer>> execute = new AsyncTask<Void, Void, List<Customer>>() {

            @Override
            protected List<Customer> doInBackground(Void... params) {
                long startTime = System.currentTimeMillis();
                List<Customer> customerList = Customer.listAll(Customer.class);
                long endCustomerTime = System.currentTimeMillis();
                mapCustomer.clear();
                for (Customer item : customerList) {
                    mapCustomer.put(item.getId(), item);
                }

                long cartStartTime = System.currentTimeMillis();
                List<Cart> cartList = Cart.listAll(Cart.class);
                long cartEndTime = System.currentTimeMillis();
                long cartLoopStartTime = System.currentTimeMillis();
                for (Cart cart : cartList) {
                    // for old db version 3 below
                    if (!cart.hasPersonId()) {
                        boolean existCustomer = false;
                        for (Customer item : customerList) {
                            if (item.name.equals(cart.getPersonName())) {
                                cart.setPersonId(item.getId());    
                                existCustomer = true;
                                break;
                            }
                        }

                        if (existCustomer == false) {
                            Customer oldCustomer = new Customer();
                            oldCustomer.initWith(cart.getPersonName(), null);
                            oldCustomer.save();
                            cart.setPersonId(oldCustomer.getId());
                            customerList.add(oldCustomer);
                            mapCustomer.put(oldCustomer.getId(), oldCustomer);
                        }
                        cart.save();                    
                    }

                    if (mapCustomer.containsKey(cart.getPersonId())) {
                        mapCustomer.get(cart.getPersonId()).addCart(cart);
                    }

                    // [2016.10.21] 초기 로딩 성능 개선, 주문 클릭시 cart.loadProductList()하게 되어 있어서 할 필요 없다
//                    cart.loadProductList();
                }
                long caratLoopEndTime = System.currentTimeMillis();

                // check duplicate name
                HashMap<String, Customer> mapCheckSameName = new HashMap<String, Customer>();
                for (Customer person : customerList) {
                    if (!mapCheckSameName.containsKey(person.name)) {
                        mapCheckSameName.put(person.name, person);
                    }
                    else {
                        // duplicate name
                        Customer duplicatePerson = mapCheckSameName.get(person.name);
                        duplicatePerson.changeCustomer(person);

                        duplicatePerson.willBeDeleted = true;
                    }
                }

                // this month
                for (int i = customerList.size() - 1; i >= 0; i--) {
                    Customer item = customerList.get(i);
                    if (item.willBeDeleted) {
                        customerList.remove(i);
                        mapCustomer.remove(item);
                        item.delete();
                    }
                    else {
                        item.updateMonthlyInfo();
                    }
                }
                long endTime = System.currentTimeMillis();
                Log.d(TAG, "[TIMESPENT][loadSaveList] Customer.listAll(Customer.class) " + (endCustomerTime-startTime) + " ms");
                Log.d(TAG, "[TIMESPENT][loadSaveList] Cart.listAll(Cart.class) " + (cartEndTime-cartStartTime) + " ms");
                Log.d(TAG, "[TIMESPENT][loadSaveList] cart loop " + (caratLoopEndTime-cartLoopStartTime) + " ms");
                Log.d(TAG, "[TIMESPENT][loadSaveList] " + (endTime-startTime) + " ms");
                return customerList;
            }

            @Override
            protected void onPostExecute(List<Customer> customerList) {
                totalPersonList.clear();
                totalPersonList.addAll(customerList);

                sortList();

                personList.clear();
                personList.addAll(totalPersonList);
                notifyDataSetChanged();
                progress.setVisibility(View.INVISIBLE);
            }

        }.execute();
        progress.setVisibility(View.VISIBLE);
    }

    public int getCustomerNumber() {
        return totalPersonList.size();
    }
    
    public int getMonthlyPV() {
        int monthlyPV = 0;
        for (Customer customer : totalPersonList) {
            monthlyPV += customer.monthlyPV;
        }
        return monthlyPV;
    }

    public int getMonthlyBV() {
        int monthlyBV = 0;
        for (Customer customer : totalPersonList) {
            monthlyBV += customer.monthlyBV;
        }
        return monthlyBV;
    }

    @Override
    public int getGroupCount() {
        return personList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return personList.get(groupPosition).getCartList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return personList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return personList.get(groupPosition).getCart(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listitem_cart_group, null);
        }

        Customer info = (Customer)getGroup(groupPosition);
        TextView name = (TextView) convertView.findViewById(R.id.textview_cart_group_person);
        TextView pv = (TextView) convertView.findViewById(R.id.textview_cart_group_person_pv);
        TextView lastDate = (TextView) convertView.findViewById(R.id.textview_cart_group_person_date);
        name.setText(info.name);
        pv.setText(loadString(R.string.monthly_pv) + " : " + getDecimalFormat(info.monthlyPV));
        lastDate.setText(dateFormat.format(new Date(info.lastDate)));

        long now = new Date().getTime();
        long diff = now - info.lastDate;
        if (diff < MONTH_1)
            lastDate.setTextColor(Color.rgb(0, 100, 0));
        else if (diff < MONTH_2)
            lastDate.setTextColor(Color.rgb(255, 127, 0));
        else
            lastDate.setTextColor(Color.RED);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listitem_cart_group_child, null);
        }

        Customer customer = (Customer)getGroup(groupPosition);
        Cart cart = (Cart)getChild(groupPosition, childPosition);
        TextView textView_title = (TextView) convertView.findViewById(R.id.textview_cart_title);
        TextView textView_date = (TextView) convertView.findViewById(R.id.textview_cart_date);
        TextView textView_brief = (TextView) convertView.findViewById(R.id.textview_cart_brief);
        TextView textView_pv_price = (TextView) convertView.findViewById(R.id.textview_cart_pv_price);

        String title = String.format("[%d] %s", customer.getCartList().size() - childPosition, cart.getTitle());
        textView_title.setText(title);
        textView_date.setText(dateFormat.format(cart.getDate()));
        textView_brief.setText(makeMultiline(cart.getBrief()));
        textView_pv_price.setText(String.format("%s: %s   %s: %s%s", loadString(R.string.pv), getDecimalFormat(cart.getPv()), loadString(R.string.price), getDecimalFormat(cart.getPrice()),loadString(R.string.currency)));

        return convertView;
    }

    private String makeMultiline(String brief) {
        if (brief == null || brief.length() == 0)
            return "";

        return  brief.replace(", ", "\n");
    }

    private void editCart(int groupPosition, int childPosition, String title) {
        Cart cart = (Cart)getChild(groupPosition, childPosition);
        cart.setTitle(title);
        cart.save();
    }

    private void deleteCart(int groupPosition, int childPosition) {
        Customer customer = (Customer)getGroup(groupPosition);
        Cart cart = customer.removeCart(childPosition);
        cart.delete();

        if (customer.getCartList().size() == 0) {
            totalPersonList.remove(customer);
            personList.remove(groupPosition);
            mapCustomer.remove(customer);
            customer.delete();
        }
    }

    public void onLongClickChild(final int groupPosition, final int childPosition) {
        final Context context = progress.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        TextView textView = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, null);
        textView.setText(R.string.confirm_edit);
        builder.setView(textView);
        builder.setNegativeButton(R.string.button_cancel, null);
        // 주문 변경
        builder.setNeutralButton(R.string.button_edit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                View dialogView = inflater.inflate(R.layout.dialog_save_cart, null);
                final AutoCompleteTextView edittext_personName = (AutoCompleteTextView) dialogView.findViewById(R.id.edittext_person_name);
                final EditText edittext_cartTitle = (EditText) dialogView.findViewById(R.id.edittext_cart_title);
                edittext_personName.setEnabled(false);
                final View linearlayout_cart_customer_memo = dialogView.findViewById(R.id.linearlayout_cart_customer_memo);
                linearlayout_cart_customer_memo.setVisibility(View.GONE);

                final Customer customer = (Customer)getGroup(groupPosition);
                final Cart cart = (Cart)getChild(groupPosition, childPosition);
                edittext_personName.setText(customer.name);
                edittext_cartTitle.setText(cart.getTitle());

                //ArrayAdapter<String> nameAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, getPersonNameList());
                //edittext_personName.setAdapter(nameAdapter);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.edit_cart);
                builder.setView(dialogView);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing
                    }
                });
                builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                final AlertDialog dialog2 = builder.create();
                dialog2.show();
                dialog2.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // final String personName = edittext_personName.getText().toString();
                        // if (personName == null || personName.length() == 0) {
                        //     Toast.makeText(context, R.string.message_input_name, Toast.LENGTH_SHORT).show();
                        //     return;
                        // }
                        final String title = edittext_cartTitle.getText().toString();
                        dialog2.dismiss();

                        editCart(groupPosition, childPosition, title);
                        sortList();
                        notifyDataSetChanged();
                        Toast.makeText(context, R.string.edit_success, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        // 주문 삭제
        builder.setPositiveButton(R.string.button_delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteCart(groupPosition, childPosition);
                sortList();
                notifyDataSetChanged();
                Toast.makeText(progress.getContext(), R.string.delete_success, Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    public void onLongClickGroup(final int groupPosition) {
        final Context context = progress.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        TextView textView = (TextView) inflater.inflate(android.R.layout.simple_list_item_1, null);
        textView.setText(R.string.confirm_edit);
        builder.setView(textView);
        builder.setNegativeButton(R.string.button_cancel, null);
        // 이름 변경
        builder.setNeutralButton(R.string.button_edit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                View dialogView = inflater.inflate(R.layout.dialog_save_cart, null);
                final AutoCompleteTextView edittext_personName = (AutoCompleteTextView) dialogView.findViewById(R.id.edittext_person_name);
                final EditText edittext_personMemo = (EditText) dialogView.findViewById(R.id.edittext_customer_memo);
                final View linearlayout_cart_title = dialogView.findViewById(R.id.linearlayout_cart_title);
                linearlayout_cart_title.setVisibility(View.GONE);

                final Customer customer = (Customer)getGroup(groupPosition);
                edittext_personName.setText(customer.name);
                edittext_personMemo.setText(customer.memo0);

                ArrayAdapter<String> nameAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, getPersonNameList());
                edittext_personName.setAdapter(nameAdapter);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.edit_cart);
                builder.setView(dialogView);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                final AlertDialog dialog2 = builder.create();
                dialog2.show();
                dialog2.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String personName = edittext_personName.getText().toString();
                        if (personName == null || personName.length() == 0) {
                            Toast.makeText(context, R.string.message_input_name, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        final String memo = edittext_personMemo.getText().toString();
                        if (personName.equals(customer.name) && memo.equals(customer.memo0)) {
                            dialog2.dismiss();
                            return;
                        }

                        progress.setVisibility(View.VISIBLE);
                        AsyncTask<Void, Void, Boolean> execute = new AsyncTask<Void, Void, Boolean>() {
                            @Override
                            protected Boolean doInBackground(Void... params) {
                                // 소비자 메모만 수정한 경우
                                if (personName.equals(customer.name) && !memo.equals(customer.memo0)) {
                                    customer.memo0 = memo;
                                    customer.save();
                                    return true;
                                }

                                // check exist same name
                                boolean existSameName = false;
                                Customer sameCustomer = null;
                                for (Customer person : totalPersonList) {
                                    if (person.name.equalsIgnoreCase(personName)) {
                                        existSameName = true;
                                        sameCustomer = person;
                                        break;
                                    }
                                }

                                // modify information
                                customer.name = personName;
                                customer.memo0 = memo;

                                if (!existSameName) {
                                    customer.save();
                                }
                                else {
                                    if (sameCustomer != null) {
                                        customer.changeCustomer(sameCustomer);

                                        personList.remove(customer);
                                        totalPersonList.remove(customer);
                                        mapCustomer.remove(customer);
                                        customer.delete();
                                        sortList();
                                    }
                                }
                                return true;
                            }

                            @Override
                            protected void onPostExecute(Boolean aBoolean) {
                                progress.setVisibility(View.INVISIBLE);
                                notifyDataSetChanged();
                                Toast.makeText(context, R.string.edit_success, Toast.LENGTH_SHORT).show();
                            }
                        }.execute();
                        dialog2.dismiss();
                    }
                });
            }
        });
        // 이름 통째로 삭제
        builder.setPositiveButton(R.string.button_delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progress.setVisibility(View.VISIBLE);
                new AsyncTask<Integer, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Integer... params) {
                        Customer customer = personList.remove(groupPosition);
                        totalPersonList.remove(customer);
                        mapCustomer.remove(customer);
                        customer.deleteAllCart();
                        customer.delete();
                        sortList();
                        return true;
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        notifyDataSetChanged();
                        progress.setVisibility(View.INVISIBLE);
                        Toast.makeText(context, R.string.delete_success, Toast.LENGTH_SHORT).show();
                    }
                }.execute(groupPosition);
            }
        });
        builder.show();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public List<String> getPersonNameList() {
        ArrayList<String> list = new ArrayList<String>();
        for (Customer customer : totalPersonList) {
            list.add(customer.name);
        }
        return list;
    }

    // public int getGroupPosition(String keyword) {
    //     for (Pair<Customer, ArrayList<Cart>> pair : personList) {
    //         if (pair.first.name.equals(keyword)) {
    //             return personList.indexOf(pair);
    //         }
    //     }
    //     return -1;
    // }

    private String getDecimalFormat(int number) {
        if (decimalFormat == null)
            decimalFormat = new DecimalFormat("###,###,###");
        return decimalFormat.format(number);
    }

    private String loadString(int id) {
        return progress.getContext().getString(id);
    }

    class NameComparator implements  Comparator<Customer> {
        private RuleBasedCollator collator;
        public NameComparator() {
            collator = (RuleBasedCollator) Collator.getInstance(Locale.KOREAN);
        }

        @Override
        public int compare(Customer lhs, Customer rhs) {
            return collator.compare(lhs.name, rhs.name);
        }
    }

    class DateComparator implements Comparator<Cart> {
        @Override
        public int compare(Cart lhs, Cart rhs) {
            return rhs.getDate().compareTo(lhs.getDate());
        }
    }

    private void sortList() {
        Collections.sort(totalPersonList, new NameComparator());
        DateComparator comparator = new DateComparator();
        for (Customer customer : totalPersonList) {
            Collections.sort(customer.getTotalCartList(), new Comparator<Cart>() {
                @Override
                public int compare(Cart lhs, Cart rhs) {
                    return rhs.getDate().compareTo(lhs.getDate());
                }
            });
            Collections.sort(customer.getCartList(), new Comparator<Cart>() {
                @Override
                public int compare(Cart lhs, Cart rhs) {
                    return rhs.getDate().compareTo(lhs.getDate());
                }
            });
        }
    }

    public Customer getPerson(Cart cart) {
        long personId = -1;
        for (Customer customer : totalPersonList) {
            if (customer.name.equalsIgnoreCase(cart.getPersonName())) {
                return customer;
            }
        }

        Customer customer = new Customer();
        customer.initWith(cart.getPersonName(), null);
        customer.addCart(cart);
        customer.save();
        return customer;
    }

    public Customer getPerson(String personName) {
        for (Customer customer : totalPersonList) {
            if (customer.name.equalsIgnoreCase(personName)) {
                return customer;
            }
        }
        return null;
    }

    public synchronized int filter(String keyword) {
        keyword = keyword.toLowerCase(Locale.getDefault());

        personList.clear();
        int firstIndex = -1;
        if (keyword.length() == 0) {
            personList.addAll(totalPersonList);
            for (Customer customer : totalPersonList) {
                customer.filter(keyword);
            }
        }
        else {
            keyword = keyword.replaceAll("\\s", "");
            for (Customer customer : totalPersonList) {
                customer.filter(keyword);
                if (customer.getCartList().size() > 0)
                    personList.add(customer);
            }

            if (personList.size() > 0)
                firstIndex = 0;
        }
        notifyDataSetChanged();
        return firstIndex;
    }

    public synchronized void loadProductList()  {
        for (Customer customer : totalPersonList) {
            customer.loadProductList();
        }
    }
}
