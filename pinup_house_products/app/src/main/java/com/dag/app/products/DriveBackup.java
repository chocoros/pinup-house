package com.dag.app.products;

import android.app.Activity;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.plus.Plus;
import com.orm.SugarConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DriveBackup {
    public interface OnDriveBackupListener {
        public void onDriveBackupResult(boolean success, boolean needReload) ;
    }

    private static final String TAG = "DriveBackup";

    public static final String FOLDER_NAME = "Pinup House";

    public static final int REQUEST_CODE_CREATOR = 1;
    public static final int REQUEST_CODE_RESOLUTION = 2;
    public static final int REQUEST_CODE_BACKUP_PERIODICALLY = 3;

    public static final String ACTION_DRIVE_BACKUP = "ACTION_DRIVE_BACKUP";

    private Activity activity;
    private OnDriveBackupListener onDriveBackupListener;
    private GoogleApiClient googleApiClient;
    private DriveId folderDriveId;

    final private ResultCallback<DriveFolder.DriveFileResult> driveFileCallback = new ResultCallback<DriveFolder.DriveFileResult>() {
        @Override
        public void onResult(DriveFolder.DriveFileResult result) {
            if (onDriveBackupListener != null)
                onDriveBackupListener.onDriveBackupResult(result.getStatus().isSuccess(), false);
        }
    };

    public DriveBackup(GoogleApiClient apiClient, Activity activity, OnDriveBackupListener onDriveBackupListener) {
        this.googleApiClient = apiClient;
        this.activity = activity;
        this.onDriveBackupListener = onDriveBackupListener;
    }

    private String getDriveBackupFolderName() {
        return "Pinup House";
    }

    private String getDataBaseName() {
        return SugarConfig.getDatabaseName(activity);
    }

    public void restoreDBFileFromDrive() {
        Query folderQuery = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, getDriveBackupFolderName()))
                .build();

        Drive.DriveApi.getRootFolder(googleApiClient).queryChildren(googleApiClient, folderQuery).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
            @Override
            public void onResult(DriveApi.MetadataBufferResult result) {
                int count = result.getMetadataBuffer().getCount();
                for (Metadata metadata : result.getMetadataBuffer()) {
                    if (metadata.getTitle().equals(getDriveBackupFolderName())) {
                        folderDriveId = metadata.getDriveId();
                        restoreDBFileFromDriveFile(folderDriveId);
                        return;
                    }
                }
                // there is no backup file
                if (onDriveBackupListener != null)
                    onDriveBackupListener.onDriveBackupResult(false, true);
            }
        });
    }

    private void restoreDBFileFromDriveFile(final DriveId folderId) {
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, getDataBaseName()))
                .build();

        // find file, if exists, copy it to db file.
        Drive.DriveApi.getFolder(googleApiClient, folderId).queryChildren(googleApiClient, query).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
            @Override
            public void onResult(DriveApi.MetadataBufferResult result) {
                int count = result.getMetadataBuffer().getCount();
                for (Metadata metadata : result.getMetadataBuffer()) {
                    if (metadata.getTitle().equals(getDataBaseName())) {
                        copyFile(metadata.getDriveId());
                        return;
                    }
                }

                String nextPageToken = result.getMetadataBuffer().getNextPageToken();
                if (nextPageToken == null) {
                    // there is no backup file
                    if (onDriveBackupListener != null)
                        onDriveBackupListener.onDriveBackupResult(false, true);
                    return;
                }

                Query nextQuery = new Query.Builder()
                        .setPageToken(nextPageToken)
                        .build();

                Drive.DriveApi.getFolder(googleApiClient, folderId).queryChildren(googleApiClient, nextQuery).setResultCallback(this);
            }
        });
    }

    public void saveDBFileToDrive() {
        Query folderQuery = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, getDriveBackupFolderName()))
                .build();

        Drive.DriveApi.getRootFolder(googleApiClient).queryChildren(googleApiClient, folderQuery).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
            @Override
            public void onResult(DriveApi.MetadataBufferResult result) {
                int count = result.getMetadataBuffer().getCount();
                for (Metadata metadata : result.getMetadataBuffer()) {
                    if (metadata.getTitle().equals(getDriveBackupFolderName())) {
                        folderDriveId = metadata.getDriveId();
                        saveDBFileToDriveFile(folderDriveId);
                        return;
                    }
                }

                MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                        .setTitle(getDriveBackupFolderName())
                        .build();

                Drive.DriveApi.getRootFolder(googleApiClient).createFolder(googleApiClient, changeSet).setResultCallback(new ResultCallback<DriveFolder.DriveFolderResult>() {
                    @Override
                    public void onResult(DriveFolder.DriveFolderResult driveFolderResult) {
                        if (driveFolderResult.getStatus().isSuccess()) {
                            folderDriveId = driveFolderResult.getDriveFolder().getDriveId();
                            saveDBFileToDriveFile(folderDriveId);
                        }
                    }
                });
            }
        });
    }

    private void saveDBFileToDriveFile(final DriveId folderId) {
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, getDataBaseName()))
                .build();

        // find file and then create or update
        Drive.DriveApi.getFolder(googleApiClient, folderId).queryChildren(googleApiClient, query).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
            @Override
            public void onResult(DriveApi.MetadataBufferResult result) {
                int count = result.getMetadataBuffer().getCount();
                for (Metadata metadata : result.getMetadataBuffer()) {
                    if (metadata.getTitle().equals(getDataBaseName())) {
                        updateFile(metadata.getDriveId());
                        return;
                    }
                }

                String nextPageToken = result.getMetadataBuffer().getNextPageToken();
                if (nextPageToken == null) {
                    createFile(folderId);
                    return;
                }

                Query nextQuery = new Query.Builder()
                        .setPageToken(nextPageToken)
                        .build();

                Drive.DriveApi.getFolder(googleApiClient, folderId).queryChildren(googleApiClient, nextQuery).setResultCallback(this);
            }
        });
    }

    private void updateFile(DriveId driveId) {
        DriveFile file = Drive.DriveApi.getFile(googleApiClient, driveId);
        new AsyncTask<DriveFile, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(DriveFile... params) {
                DriveFile file = params[0];
                try {
                    DriveApi.DriveContentsResult driveContentsResult = file.open(googleApiClient, DriveFile.MODE_WRITE_ONLY, null).await();
                    if (!driveContentsResult.getStatus().isSuccess()) {
                        return false;
                    }

                    DriveContents driveContents = driveContentsResult.getDriveContents();
                    OutputStream outputStream = driveContents.getOutputStream();
                    writeFile(outputStream);
                    com.google.android.gms.common.api.Status status = driveContents.commit(googleApiClient, null).await();
                    return status.getStatus().isSuccess();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (onDriveBackupListener != null)
                    onDriveBackupListener.onDriveBackupResult(result, false);
            }
        }.execute(file);
    }

    private void createFile(final DriveId folderId) {
        Drive.DriveApi.newDriveContents(googleApiClient).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
            @Override
            public void onResult(DriveApi.DriveContentsResult result) {
                // If the operation was not successful, we cannot do anything
                // and must
                // fail.
                if (!result.getStatus().isSuccess()) {
                    Log.i(TAG, "Failed to create new contents.");
                    return;
                }
                // Otherwise, we can write our data to the new contents.
                Log.i(TAG, "New folder created.");
                try {
                    writeFile(result.getDriveContents().getOutputStream());
                } catch (IOException e1) {
                    Log.i(TAG, "Unable to write file contents.");
                }
                // Create the initial metadata - MIME type and title.
                // Note that the user will be able to change the title later.
                MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
                        .setMimeType("application/x-sqlite3")
                        .setTitle(getDataBaseName())
                        .build();

                Drive.DriveApi.getFolder(googleApiClient, folderId)
                        .createFile(googleApiClient, metadataChangeSet, result.getDriveContents())
                        .setResultCallback(driveFileCallback);
            }
        });
    }

    private void copyFile(DriveId driveId) {
        DriveFile file = Drive.DriveApi.getFile(googleApiClient, driveId);
        new AsyncTask<DriveFile, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(DriveFile... params) {
                DriveFile file = params[0];
                try {
                    DriveApi.DriveContentsResult driveContentsResult = file.open(googleApiClient, DriveFile.MODE_READ_ONLY, null).await();
                    if (!driveContentsResult.getStatus().isSuccess()) {
                        return false;
                    }

                    DriveContents driveContents = driveContentsResult.getDriveContents();
                    InputStream inputStream = driveContents.getInputStream();
                    readFile(inputStream);
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                if (onDriveBackupListener != null)
                    onDriveBackupListener.onDriveBackupResult(result, true);
            }
        }.execute(file);
    }

    private void writeFile(OutputStream outputStream) throws IOException {
        File dbFile = activity.getDatabasePath(SugarConfig.getDatabaseName(activity));
        FileInputStream inputStream = new FileInputStream(dbFile);

        int readCount = 0;
        byte[] buffer = new byte[1024];
        while((readCount = inputStream.read(buffer,0,1024))!= -1){
            outputStream.write(buffer,0,readCount);
        }
        inputStream.close();
    }

    private void readFile(InputStream inputStream) throws IOException {
        File dbFile = new File(activity.getDatabasePath(getDataBaseName()).getAbsolutePath());
        dbFile.getParentFile().mkdir();

        FileOutputStream outputStream = new FileOutputStream(dbFile);

        int readCount = 0;
        byte[] buffer = new byte[1024];
        while((readCount = inputStream.read(buffer,0,1024))!= -1){
            outputStream.write(buffer,0,readCount);
        }
        inputStream.close();
    }
}