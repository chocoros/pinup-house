package com.dag.app.products;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.text.SimpleDateFormat;

public class KeyValue extends SugarRecord<KeyValue> {

    private String key;
    private String value;
    private String updateDate;

    // keys
    @Ignore
    public final static String KEY_SUBS_TYPE = "subs_type";

    @Ignore
    public final static String KEY_SUBS_EXPIRY = "subs_expiry";

    @Ignore
    public final static String KEY_UPDATE_PRODUCTS = "update_products";

    @Ignore
    public final static String KEY_IAB_RESULT_RESPONSE = "iab_response";

    @Ignore
    public final static String KEY_IAB_RESULT_MESSAGE = "iab_message";

    @Ignore
    public final static String KEY_PURCHASE_INFO = "purchase_info";

    // types
    @Ignore
    public final static String INAPP_ITEM_SUBS_FREE_USER = "subscription_free_user";

    @Ignore
    public final static String INAPP_ITEM_SUBS_MONTH_1 = "subscription_month_1";

    @Ignore
    public final static String INAPP_ITEM_SUBS_YEAR_1 = "subscription_year_1";

    // utility
    @Ignore
    public final static SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public KeyValue() {
    }

    public void initWith(String key, String value, String updateDate) {
        this.key = key;
        this.value = value;
        this.updateDate = updateDate;
    }

    public String getKey() { return key; }
    public String getValue() { return value; }
    public String getUpdateDate() { return updateDate; }
}
