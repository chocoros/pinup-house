package com.dag.app.products;

import android.database.sqlite.SQLiteException;
import android.util.Pair;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Cart extends SugarRecord<Cart> {
    private String personName;
    private String title;
    private Date date;
    private int pv;
    private int bv;
    private int price;
    private String brief;
    private long personId;

    @Ignore
    private List<CartProductListRecord> productList;

    public Cart() {
        personName = "";
        title = "";
        date = null;
        pv = 0;
        bv = 0;
        brief = "";
        personId = -1;
        productList = new ArrayList<CartProductListRecord>();
    }

    public void initWith(String personName, String title, Date date, int pv, int bv, int price, String brief, long personId) {
        this.personName = personName;
        this.title = title;
        this.pv = pv;
        this.bv = bv;
        this.price = price;
        this.date = date;
        this.brief = brief;
        this.personId = personId;
    }

    public void setPersonName(String name) { this.personName = name; }

    public String getPersonName() {
        return personName;
    }

    public void setTitle(String title) { this.title = title; }

    public String getTitle() {
        return title;
    }

    public Date getDate() {
        return date;
    }

    public int getPv() {
        return pv;
    }

    public int getBv() {
        return bv;
    }

    public int getPrice() {
        return price;
    }

    public String getBrief() {
        return brief;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public long getPersonId() {
        return personId;
    }

    public boolean hasPersonId() {
        return personId != -1;
    }

    public List<CartProductListRecord> getProductList() {
        return productList;
    }

    public void saveProductList(List<Pair<Product, Integer>> list) {
        productList.clear();
        for (Pair<Product, Integer> cart : list) {
            long orderId = getId();
            CartProductListRecord record = new CartProductListRecord();
            Product product = cart.first;
            record.initWith(orderId, product.getSKU(), cart.second, product.getName(), product.getVPS(), product.getPV(), product.getBV(), product.getPrice(), product.getBrand(), product.getSubbrand(), product.getPromotion(), product.getImageSrc(), product.getAlias());
            productList.add(record);
        }

        CartProductListRecord.saveInTx(productList);
    }

    public boolean loadProductList() {
        try {
            productList.clear();
            long orderId = getId();
            List<CartProductListRecord> list = CartProductListRecord.find(CartProductListRecord.class, "orderId = ?", String.valueOf(orderId));
            if (list.size() == 0)
                return false;

            productList.addAll(list);
            for (CartProductListRecord product : list) {
                product.makeComparisonData();
            }
            return true;
        }
        catch (SQLiteException e) {
            e.printStackTrace();
        }
        return false;
    }
}
