package com.dag.app.products;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ToggleButton;

import com.dag.web.products.myApi.model.VersionInfo;

import java.util.ArrayList;
import java.util.List;

public class ProductFragment extends Fragment {

    private ProductListAdapter adapter;
    private ToggleButton toggleSelected;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.layout_manage_products, container, false);
    	initData();
		setEventListener(view);
        startLoading();
        return view;
    }

    private void initData() {
        // List
        adapter = new ProductListAdapter(getActivity());

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
    }

    private void setEventListener(View parent) {
        // List
        ListView listView = (ListView) parent.findViewById(R.id.listview_products);
        listView.setAdapter(adapter);

        View totalView = parent.findViewById(R.id.linearylayout_product_total);
        adapter.setProductTotalTextView(totalView);

        // Buttons
        toggleSelected = (ToggleButton) parent.findViewById(R.id.togglebutton_product_show_selected);
        toggleSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                adapter.showSelected(isChecked);
            }
        });

        View deleteSelected = parent.findViewById(R.id.imagebutton_product_delete_selected);
        deleteSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.deletedSelected();
                toggleSelected.setChecked(false);
            }
        });

        // load list
        Database.getInstance().setOnDataBaseListener(new Database.DataBaseListener() {
            @Override
            public void onProductListCreated(List<Product> list, boolean clearCache) {
                setProductList(list, clearCache);
                progressDialog.dismiss();
            }

            @Override
            public void onRequestGoogleApiClientConnection(boolean isSave, boolean isRestore) {
            }

            @Override
            public void onProgressMessageChanged(int id) {
                if (progressDialog != null && getActivity() != null)
                    progressDialog.setMessage(getActivity().getString(id) + "...");
            }

            @Override
            public void onVersionInfoReceived(VersionInfo info) {

            }
        });
    }

    private void startLoading() {
        progressDialog.setMessage(getActivity().getString(R.string.loading) + "...");
        progressDialog.show();

        Database.getInstance().requestProductList(getActivity());
    }

    public void setShowcaseViewMode(boolean enabled) {
    	adapter.setShowcaseViewMode(enabled);
    }

    public int getProductCount() {
    	return adapter.getCount();
    }

    public void filter(String keyword) {
        if (adapter != null)
    	   adapter.filter(keyword);

		if (toggleSelected != null && toggleSelected.isChecked()) {
			toggleSelected.setChecked(false);
		}
    }

    private void setProductList(List<Product> products, boolean clearCache) {
        // exception
        if (products == null)
            return;

        if (clearCache)
           adapter.clearCache();
        adapter.setProductList(products);
        adapter.updateProductTotal();
    }

    public List<Product> getSelected() {
        if (adapter != null)
    	   return adapter.getSelected();
        return new ArrayList<Product>();
    }

    public void selectProductsByCart(Cart cart, boolean clean) {
        if (adapter.selectProductsByCart(cart, clean)) {
            toggleSelected.setChecked(false);
            toggleSelected.setChecked(true);
        }
    }

    public void decreaseTextSize() {
    	adapter.decreaseTextSize();
    }

    public void increaseTextSize() {
    	adapter.increaseTextSize();
    }
}