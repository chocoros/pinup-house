/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package com.dag.web.products;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.repackaged.org.codehaus.jackson.util.ByteArrayBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.logging.Level;

import javax.inject.Named;

import sun.net.www.content.text.PlainTextInputStream;

/**
 * An endpoint class we are exposing
 */
@Api(name = "myApi", version = "v1", namespace = @ApiNamespace(ownerDomain = "products.web.dag.com", ownerName = "products.web.dag.com", packagePath = ""))
public class MyEndpoint {
    static boolean INITIALIZED = false;

    final String KIND_PRODUCT = "PRODUCT";
    final String KEY_PRODUCT = "PRODUCT";
    final String PROP_PRODUCT = "PRODUCT";

    // ver2
    final String KIND_PRODUCT2 = "PRODUCTJSON";
    final String KEY_PRODUCT2 = "PRODUCTJSON";
    final String PROP_PRODUCT2 = "PRODUCTJSON";

    final int VERSIONCODE = 32;
    final int PRODUCTVERSION = 2017071401;

    /**
     * A simple endpoint method that takes a name and says Hi back
     */
    @ApiMethod(name = "sayHi")
    public ProductList sayHi(@Named("name") String name) {
        ProductList response = new ProductList();
        response.setData("Hi, " + name);

        return response;
    }

    @ApiMethod(name = "getProductList", path = "get_productlist")
    public ProductList getProductList() {
        if (!INITIALIZED) {
            INITIALIZED = fetchProductList(1, KEY_PRODUCT, KIND_PRODUCT, PROP_PRODUCT);
        }

        ProductList response = new ProductList();

        String products = loadProductList(1, KEY_PRODUCT, KIND_PRODUCT, PROP_PRODUCT);
        if (products == null) {
            response.setBytes("{[]}".getBytes());
            return response;
        }
        response.setBytes(products.getBytes(Charset.forName("UTF-8")));
        return response;
    }

    @ApiMethod(name = "getProductListFromJSON", path = "get_productlist_from_json")
    public ProductList getProductListFromJSON() {
        if (!INITIALIZED) {
            INITIALIZED = fetchProductList(2, KEY_PRODUCT2, KIND_PRODUCT2, PROP_PRODUCT2);
        }

        ProductList response = new ProductList();

        String products = loadProductList(2, KEY_PRODUCT2, KIND_PRODUCT2, PROP_PRODUCT2);
        if (products == null) {
            response.setBytes("{[]}".getBytes());
            return response;
        }
        response.setBytes(products.getBytes(Charset.forName("UTF-8")));
        return response;
    }

    @ApiMethod(name = "getVersion", path = "get_version")
    public VersionInfo getVersion() {
        VersionInfo version = new VersionInfo();
        version.versionCode = VERSIONCODE;
        version.productVersion = PRODUCTVERSION;
        return version;
    }

    private String loadProductList(int version, String paramKey, String paramKind, String paramProp) {
        MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
        Text products = (Text) syncCache.get(paramKey.getBytes());
        if (products == null) {
            try {
                DatastoreService service = DatastoreServiceFactory.getDatastoreService();
                Key key = KeyFactory.createKey(paramKind, paramKey);
                Entity entity = service.get(key);
                products = (Text)entity.getProperty(paramProp);

                syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
                syncCache.put(paramKey.getBytes(), products);
            } catch (EntityNotFoundException e) {
                e.printStackTrace();
            }

            if (products == null) {
                if (!fetchProductList(version, paramKey, paramKind, paramProp)) {
                    return null;
                }

                products = (Text)syncCache.get(paramKey.getBytes());
                if (products == null) {
                    return null;
                }
            }
        }
        return products.getValue();
    }

    private boolean fetchProductList(int version, String paramKey, String paramKind, String paramProp) {
        try {
            String path = "products/product_list.txt";
            if (version == 2)
                path = "products/product_list2.txt";    // json file

            InputStream inputStream = new FileInputStream(new File(path));
            if (inputStream == null)
                return false;

            String json = "";
            if (version == 1) {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                while (inputStream.read(buffer) > 0) {
                    os.write(buffer);
                }

                byte[] content = os.toByteArray();
                os.close();

                ArrayList<Product> productList = new ArrayList<Product>();
                String products = new String(content, "UTF-8");
                BufferedReader reader = new BufferedReader(new StringReader(products));
                String line = null;
                try {
                    line = reader.readLine();
                    while ((line = reader.readLine()) != null) {
                        Product product = toProduct(line);
                        if (product != null) {
                            productList.add(product);
                        }
                    }
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                GsonBuilder builder = new GsonBuilder();
                builder.setPrettyPrinting();
                Gson gson = builder.create();
                java.lang.reflect.Type collectionType = new TypeToken<ArrayList<Product>>(){}.getType();
                json = gson.toJson(productList, collectionType);
            }
            else if (version == 2) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                try {
                    StringBuilder sb = new StringBuilder();
                    String line  = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\r\n");
                        //json += line + "\r\n";
                    }
                    json = sb.toString();
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            inputStream.close();

            Text textData = new Text(json);

            // save to Datastore
            Entity entity = new Entity(paramKind, paramKey);
            entity.setProperty(paramProp, textData);
            DatastoreService service = DatastoreServiceFactory.getDatastoreService();
            service.put(entity);

            // keep memcache
            MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
            syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
            syncCache.put(paramKey.getBytes(), textData);
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Product toProduct(String line) {
        try {
            StringTokenizer token = new StringTokenizer(line, ",");

            String name = token.nextToken();
            String sku = token.nextToken();
            String vps = token.nextToken();
            String pv = token.nextToken();
            String bv = token.nextToken();
            String price = token.nextToken();
            String brand = token.nextToken();
            String category1 = "";
            String category2  = "";
            String category3 = "";
            if (token.hasMoreTokens())
                category1 = token.nextToken();
            if (token.hasMoreTokens())
                category2 = token.nextToken();
            if (token.hasMoreTokens())
                category3 = token.nextToken();
            String alias = "";
            if (token.hasMoreTokens())
                alias = token.nextToken();

            Product product = new Product();
            product.name = name;
            product.sku = sku;
            product.vps = vps;
            product.pv = pv;
            product.bv = bv;
            product.price = price;
            product.brand = brand;
            product.category1 = category1;
            product.category2 = category2;
            product.category3 = category3;
            product.alias = alias;

            return product;
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        return null;
    }
}
