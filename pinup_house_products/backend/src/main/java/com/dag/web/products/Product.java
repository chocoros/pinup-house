package com.dag.web.products;

/**
 * The object model for the data we are sending through endpoints
 */
public class Product {
    public String name;
    public String sku;
    public String vps;
    public String pv;
    public String bv;
    public String price;
    public String brand;
    public String category1;
    public String category2;
    public String category3;
    public String alias;
}
