package com.dag.web.products;

/**
 * The object model for the data we are sending through endpoints
 */
public class ProductList {

    private String data;
    private byte[] byteStream;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setBytes(byte[] stream) { this.byteStream = stream; }

    public byte[] getBytes() { return this.byteStream; }
}