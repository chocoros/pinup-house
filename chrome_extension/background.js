// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Called when the url of a tab changes.
function checkForValidUrl(tabId, changeInfo, tab) {
  // If the letter 'business/losmap' is found in the tab's URL...
  if (tab.url.indexOf('losmap/business-results') > -1) {
    // ... show the page action.
    chrome.pageAction.show(tabId);
  }
};

function onClickedPageActionIcon(tab) {
	chrome.tabs.query({active:true, currentWindow:true}, function(tabs) {
		chrome.tabs.sendMessage(tabs[0].id, {action:'showAll'}, function(response) {
			alert('finish send message to content script');
		});
	});
}

// Listen for any changes to the URL of any tab.
chrome.tabs.onUpdated.addListener(checkForValidUrl);

// Listen for click page action icon
chrome.pageAction.onClicked.addListener(onClickedPageActionIcon);

