// message from background.js
chrome.extension.onMessage.addListener(function(msg, sender, sendResponse) {
	if (msg.action == 'showAll') {
		expandAndShowAll();
		//PrintLosmap();
	}
});

function clickLink(element) {
	if (element == null)
		return;

	if (element.click) {
		element.click();
	}
	else {
		var clickEvent = document.createEvent('MouseEvents');
		clickEvent.initEvent('click', true, true);
		element.dispatchEvent(clickEvent);
	}
}

function expandAll() {
	var exist = false;
	var treegridList = document.querySelectorAll('.treegrid_cell');
	for (var i = 0; i < treegridList.length; i++) {
		var treenode = treegridList[i];
		var imgList = treenode.querySelectorAll('img');
		for (var j = 0 ; j < imgList.length; j++) {
			var imgtag = imgList[j];
			if (imgtag.src.endsWith('plus.gif')) {
				exist = true;
				clickLink(imgtag);
			}	
		}
	}
	return exist;
}

function expandOne() {
	var exist = false;
	var treegridList = document.querySelectorAll('.treegrid_cell');
	for (var i = 0; i < treegridList.length; i++) {
		var treenode = treegridList[i];
		var imgList = treenode.querySelectorAll('img');
		for (var j = 0 ; j < imgList.length; j++) {
			var imgtag = imgList[j];
			if (imgtag.src.endsWith('plus.gif')) {
				exist = true;
				clickLink(imgtag);
				break;
			}	
		}
	}
	return exist;
}


function checkNetworkComplete() {
	var complete = true;
	var nodevalList = document.querySelectorAll('#nodeval');
	for (var i = 0; i < nodevalList.length; i++) {
		var nodeval = nodevalList[i];
		if (nodeval.innerText.startsWith('Loading')) {
			complete = false;
			break;
		}
	}
	return complete;
}

function endExpandAll() {
	var r = confirm("완료. 출력하시겠습니까?");
	if (r == true) {
		PrintLosmap();
	} else {
	}
}

function addFunctions() {
	if (!String.prototype.startsWith) {
	    Object.defineProperty(String.prototype, 'startsWith', {
	        enumerable: false,
	        configurable: false,
	        writable: false,
	        value: function (searchString, position) {
	            position = position || 0;
	             return this.indexOf(searchString, position) === position;
	        }
	    });
	}

	if (!String.prototype.endsWith) {
	    Object.defineProperty(String.prototype, 'endsWith', {
	        enumerable: false,
	        configurable: false,
	        writable: false,
	        value: function (searchString) {
	             return this.indexOf(searchString)+searchString.length === this.length;
	        }
	    });
	}  
}

function expandAndShowAll() {
	addFunctions();

	var exist = expandOne();
	if (!exist) {
		endExpandAll();
		return;
	}

	var timer = setInterval(function(){
		if (checkNetworkComplete()) {
			var exist = expandOne();
			if (!exist) {
				clearInterval(timer);
				endExpandAll();
				return;
			}
		}
	}, 2000);
}

function PrintLosmap() {
	// remove scrollbar
	var losmap = document.getElementById("losmap");
    losmap.removeAttribute("height");
    losmap.removeAttribute("width");
    losmap.style.height = null;	
	losmap.style.width = null;

    var objbox = losmap.querySelectorAll(".objbox")[0];
    objbox.style.height = null;
    objbox.style.width = null;

	PrintElem(losmap.parentElement);
}

function PrintElem(elem) {
	// Popup($(elem).html());
	Popup(elem.innerHTML);
}

function Popup(data) {
    var mywindow = window.open('', 'LOSMAP', 'height=800,width=800');
    mywindow.document.write('<html><head><title>LOSMAP</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    var cssList = document.getElementsByTagName("link");
    for (var i = 0; i < cssList.length; i++) {
    	var css = cssList[i];
    	mywindow.document.write(css.outerHTML);
    }
    

    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    // mywindow.close();

    return true;
}