function sortAndReduce(list) {
    var uniq = list.slice() // 정렬하기 전에 복사본을 만든다.
    .sort(function(a,b){
        return a.vps - b.vps;
    })
    .reduce(function(a,b){
        var aObject = a.slice(-1)[0];
        if (aObject == undefined || aObject.sku !== b.sku) a.push(b); // slice(-1)[0] 을 통해 마지막 아이템을 가져온다.
        return a;
    },[]); //a가 시작될 때를 위한 비어있는 배열

    // 한 줄로 표현
    return list.slice().sort(function(a,b){return a.vps - b.vps}).reduce(function(a,b){var aObject = a.slice(-1)[0];if (aObject == undefined || aObject.sku !== b.sku) a.push(b);return a;},[]);
}

function addCustomProducts(prodAllList) {
    var item = {};
    item["name"] = "커스터마이즈드 맨즈 에너지 팩";
    item["sku"] = "117872K";
    item["vps"] = "2064";
    item["pv"] = "99680";
    item["bv"] = "154500";
    item["price"] = "170000"
    item["brand"] = "뉴트리라이트";
    item["subbrand"] = "커스터마이즈드팩";
    item["promotion"] = "NO";
    item["imagesrc"] = "https://www.abnkorea.co.kr/medias/NU-117872K-156.png?context=bWFzdGVyfHByb2R1Y3QtZ2FsbGVyeS1pbWFnZXN8MzQ1NDV8aW1hZ2UvcG5nfHByb2R1Y3QvZ2FsbGVyeS9pbWFnZXMvaDRmL2g2Zi84ODQ5MDc5Nzk1NzQyLnBuZ3w3ZDQ4ZmZhYTQzYzQ1OWZkOTQ1MTk0OTIxOGQ4MWY1YTg1ZjNlZTA5ZmRhYTNkYjU1NTU1MjI5YzFiYmRmYzhk";
    item["aliasname"] = "#";
    prodAllList.push(item);

    item = {};
    item["name"] = "커스터마이즈드 우먼즈 바이탈 팩";
    item["sku"] = "117873K";
    item["vps"] = "2065";
    item["pv"] = "83880";
    item["bv"] = "130000";
    item["price"] = "143000"
    item["brand"] = "뉴트리라이트";
    item["subbrand"] = "커스터마이즈드팩";
    item["promotion"] = "NO";
    item["imagesrc"] = "https://www.abnkorea.co.kr/medias/NU-117873K-156.png?context=bWFzdGVyfHByb2R1Y3QtZ2FsbGVyeS1pbWFnZXN8MzQ0ODR8aW1hZ2UvcG5nfHByb2R1Y3QvZ2FsbGVyeS9pbWFnZXMvaDI5L2g2Zi84ODQ5MDgwMzg1NTY2LnBuZ3xmOGNmNmU4Yjk2YzYxMjhmNGExNGI1NmJmZTY0OGE2ZTBhNGFmZjg2NzQ2YzUzY2E3NGJmMzdkZDM2ZWQ3Mjdl";
    item["aliasname"] = "#";
    prodAllList.push(item);

    item = {};
    item["name"] = "커스터마이즈드 골든 팩";
    item["sku"] = "117874K";
    item["vps"] = "2066";
    item["pv"] = "109100";
    item["bv"] = "169100";
    item["price"] = "186000"
    item["brand"] = "뉴트리라이트";
    item["subbrand"] = "커스터마이즈드팩";
    item["promotion"] = "NO";
    item["imagesrc"] = "https://www.abnkorea.co.kr/medias/NU-117874K-156.png?context=bWFzdGVyfHByb2R1Y3QtZ2FsbGVyeS1pbWFnZXN8MzI5NjJ8aW1hZ2UvcG5nfHByb2R1Y3QvZ2FsbGVyeS9pbWFnZXMvaDQ2L2hlZS84ODQ5MDgwOTc1MzkwLnBuZ3xjMDlmY2M3OWI5NTczYzNjN2RiODMwMDQzMzk0ZTg5OWZiMDdhZDk2MWM5MWFmM2Y0NGJlYTdlOWM4MGE4ODky";
    item["aliasname"] = "#";
    prodAllList.push(item);

    item = {};
    item["name"] = "커스터마이즈드 틴즈 퍼펙트 팩";
    item["sku"] = "117875K";
    item["vps"] = "2067";
    item["pv"] = "81540";
    item["bv"] = "126400";
    item["price"] = "139000"
    item["brand"] = "뉴트리라이트";
    item["subbrand"] = "커스터마이즈드팩";
    item["promotion"] = "NO";
    item["imagesrc"] = "https://www.abnkorea.co.kr/medias/NU-117875K-156.png?context=bWFzdGVyfHByb2R1Y3QtZ2FsbGVyeS1pbWFnZXN8MzQ4NjF8aW1hZ2UvcG5nfHByb2R1Y3QvZ2FsbGVyeS9pbWFnZXMvaDA1L2hlYy84ODQ5MDgxNTY1MjE0LnBuZ3w3YmNmMGIyZjVlYTA2N2JkNGMxYzEyOWU5ODFlZDJjOGExNjcxNDQ5ZDNiZmQ0YTc2YTAwMzQ4NDBhZjRjYTRi";
    item["aliasname"] = "#";
    prodAllList.push(item);

    prodAllList = sortAndReduce(prodAllList);
    localStorage.prodAllList = JSON.stringify(prodAllList, null, 4);
}

function clearProductList() {
    localStorage.removeItem("prodAllList");
}

function extractProductList() {
    var prodAllList = new Array();
    if (localStorage.prodAllList != null) {
        prodAllList = JSON.parse(localStorage.prodAllList);
    }

    var proListWrap = document.getElementById("proListWrap");
    var proListType = proListWrap.querySelector(".proListType");
    var liList = proListType.getElementsByTagName("li");
    for (var i = 0; i < liList.length; i++) {
        var li = liList[i];

        var prom = li.querySelector(".prom");
        var img = li.querySelector(".thumbLink").querySelector("a").querySelector("img");
        var dataBinding = li.querySelector("#dataBinding");
        var item = {};
        item["name"] = dataBinding.querySelector("#name").innerText;
        item["sku"] = dataBinding.querySelector("#code").innerText;     // ID
        item["vps"] = img.alt.substring(1, 5);                          // "[2276]110607K3이름"
        item["pv"] = dataBinding.querySelector("#pv").innerText;
        item["bv"] = dataBinding.querySelector("#bv").innerText;
        item["price"] = dataBinding.querySelector("#price").innerText;    
        item["brand"] = dataBinding.querySelector("#brand").innerText;
        item["subbrand"] = dataBinding.querySelector("#subBrand").innerText;    // CATEGORY 1
        item["promotion"] = prom != null ? "YES" : "NO";
        item["imagesrc"] = img.src;
        item["aliasname"] = "#";
        prodAllList.push(item);
    }

    console.log("[extractProductList] prodAllList.length = " + prodAllList.length);
    prodAllList = sortAndReduce(prodAllList);
    console.log("[extractProductList] prodAllList.length = " + prodAllList.length + " with sorted");

    localStorage.prodAllList = JSON.stringify(prodAllList, null, 4);
}

function downloadProductList() {
    var prodAllList = new Array();
    if (localStorage.prodAllList != null) {
        prodAllList = JSON.parse(localStorage.prodAllList);
    }

    addCustomProducts(prodAllList);
    // download    
    var textToWrite = JSON.stringify(prodAllList, null, 4);
    var textFileAsBlob = new Blob([textToWrite], { type:'text/plain'} );
    var fileNameToSaveAs = "ProductList.txt";

    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innterHTML = "Download File";
    downloadLink.href =  window.URL.createObjectURL(textFileAsBlob);
    downloadLink.click();
}

function startProductList() {
    clearProductList();
    extractProductList();    
}

function continueProductList() {
    extractProductList();    
}

//extractProductList();
//downloadProductList();