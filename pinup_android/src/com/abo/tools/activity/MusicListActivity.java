package com.abo.tools.activity;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.abo.tools.R;
import com.abo.tools.utils.MusicItem;
import com.abo.tools.utils.MusicListDB;
import com.abo.tools.utils.MusicListDB.Sort;
import com.abo.tools.utils.TestDataMaker;

public class MusicListActivity extends FragmentActivity implements
		ActionBar.TabListener {

	private ListView mMusicListView;
	private MusicListAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_music_list);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayHomeAsUpEnabled(true);

		String[] tabTitles = getResources().getStringArray(
				R.array.tab_music_list);
		for (int i = 0; i < tabTitles.length; i++) {
			actionBar.addTab(actionBar.newTab().setText(tabTitles[i])
					.setTabListener(this));
		}

		mMusicListView = (ListView) findViewById(R.id.music_list);
		mAdapter = new MusicListAdapter();
		mMusicListView.setAdapter(mAdapter);
		mMusicListView.setOnItemClickListener(mAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.music_list_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		case R.id.action_sort:
			sortMusicList();
			break;
		case R.id.action_make_test_data:
			makeMusicDummyData();
			break;
		case R.id.action_delete_test_data:
			deleteMusicData();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	private void sortMusicList() {
		final CharSequence[] items = { "title", "speaker", "author", "date" };

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Sort Type");
		builder.setSingleChoiceItems(items, 0,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Sort type = Sort.values()[which];
						mAdapter.sort(type);

						dialog.dismiss();
					}
				});
		builder.show();
	}

	private void makeMusicDummyData() {
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getText(R.string.action_make_test_data_message));

		TestDataMaker maker = new TestDataMaker(MusicListAdapter.DB_PATH,
				new TestDataMaker.OnResultListener() {

					@Override
					public void onFinished(String dbPath, int count) {
						progress.dismiss();
						mAdapter.notifyDataSetChanged();
					}
				});

		maker.makeAsync(this, 1000);
		progress.show();
	}

	private void deleteMusicData() {
		final ProgressDialog progress = new ProgressDialog(this);
		progress.setMessage(getText(R.string.action_delete_data_message));

		TestDataMaker maker = new TestDataMaker(MusicListAdapter.DB_PATH,
				new TestDataMaker.OnResultListener() {

					@Override
					public void onFinished(String dbPath, int count) {
						progress.dismiss();
						mAdapter.notifyDataSetChanged();
					}
				});

		maker.deleteAsync(this);
		progress.show();
	}

	class MusicListAdapter extends BaseAdapter implements OnItemClickListener,
			OnClickListener {

		public static final String DB_PATH = "musiclist.db";

		private MusicListDB mDB;
		private View mMusicIconButton;
		private View mGotoPlayButton;
		private MusicListDB.Sort mSort = MusicListDB.Sort.TITLE;

		public MusicListAdapter() {
			mDB = new MusicListDB(MusicListActivity.this, DB_PATH);
		}

		public void sort(MusicListDB.Sort type) {
			mSort = type;
			notifyDataSetChanged();
		}

		@Override
		public void notifyDataSetChanged() {
			mDB.refresh(mSort);
			super.notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return mDB.getCount();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(
						R.layout.item_music_list, null);
			}

			TextView title = (TextView) convertView.findViewById(R.id.title);
			TextView totaltime = (TextView) convertView
					.findViewById(R.id.totaltime);
			TextView speaker = (TextView) convertView
					.findViewById(R.id.speaker);
			TextView author = (TextView) convertView.findViewById(R.id.author);

			MusicItem item = mDB.get(position);
			title.setText(item.title);
			totaltime.setText(String.valueOf(item.totalTime));
			speaker.setText(item.speaker);
			author.setText(item.author);

			mGotoPlayButton = convertView.findViewById(R.id.goto_play);
			mGotoPlayButton.setOnClickListener(this);

			mMusicIconButton = convertView.findViewById(R.id.music_icon);
			mMusicIconButton.setOnClickListener(this);
			return convertView;
		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {

		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.goto_play:
				Intent intent = new Intent(getApplicationContext(),
						MusicPlayerActivity.class);
				startActivity(intent);
				break;
			case R.id.music_icon:
				PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
				Menu menu = popupMenu.getMenu();
				popupMenu.getMenuInflater().inflate(
						R.menu.music_list_item_menu, menu);
				popupMenu.show();
				break;
			default:
				break;
			}
		}
	}

}
