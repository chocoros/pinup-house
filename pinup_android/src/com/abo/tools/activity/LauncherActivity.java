package com.abo.tools.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.abo.tools.R;

public class LauncherActivity extends FragmentActivity implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_launcher);

		getActionBar().setDisplayShowTitleEnabled(false);
		getActionBar().setDisplayUseLogoEnabled(false);
		getActionBar().setDisplayHomeAsUpEnabled(false);
		getActionBar().setDisplayShowHomeEnabled(false);

		initilizeListener();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.launcher_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_settings:
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void initilizeListener() {
		findViewById(R.id.launcher_menu_1).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.launcher_menu_1:
		case R.id.launcher_menu_2:
		case R.id.launcher_menu_3:
		case R.id.launcher_menu_4:
		case R.id.launcher_menu_5:
		case R.id.launcher_menu_6:
			moveActivity(v.getId());
			break;
		default:
			break;
		}
	}

	private void moveActivity(int menuId) {
		Intent intent = new Intent();
		switch (menuId) {
		case R.id.launcher_menu_1:
			intent.setClass(this, MusicPlayerActivity.class);
			break;
		case R.id.launcher_menu_2:
			break;
		case R.id.launcher_menu_3:
			break;
		case R.id.launcher_menu_4:
			break;
		case R.id.launcher_menu_5:
			break;
		case R.id.launcher_menu_6:
			break;
		default:
			break;
		}

		startActivity(intent);
	}
}
