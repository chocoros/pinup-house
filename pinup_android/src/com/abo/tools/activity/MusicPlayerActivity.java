package com.abo.tools.activity;

import java.util.Calendar;

import android.app.ActionBar;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.abo.tools.R;
import com.abo.tools.musicplayer.PlaybarMonitor;

public class MusicPlayerActivity extends FragmentActivity implements
		PlaybarMonitor.OnProgressListener {

	private MediaPlayer mMediaPlayer;
	private SeekBar mSeekBar;
	private TextView mProgressTime;
	private TextView mProgressMax;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_music_player);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		mSeekBar = (SeekBar) findViewById(R.id.player_seekbar);
		mProgressTime = (TextView) findViewById(R.id.player_progress_text);
		mProgressMax = (TextView) findViewById(R.id.player_progress_max_text);

		mMediaPlayer = new MediaPlayer();
		new PlaybarMonitor(mSeekBar, mMediaPlayer, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.music_player_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			break;
		case R.id.action_music_list:
			moveActivity(MusicListActivity.class);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMediaPlayer != null) {
			mMediaPlayer.release();
		}
	}

	private void moveActivity(Class<?> clazz) {
		Intent intent = new Intent(getApplicationContext(), clazz);
		startActivity(intent);
	}

	/** Called by button in the layout */
	public void onClickPlayButton(View view) {
		togglePlayButtonState((ImageButton) view);

		loadAudio();
		playPauseAudio();
	}

	private void togglePlayButtonState(ImageButton v) {
		if (mMediaPlayer.isPlaying()) {
			v.setBackgroundResource(R.drawable.player_button_pause);
		} else {
			v.setBackgroundResource(R.drawable.player_button_play);
		}
	}

	private void loadAudio() {
		if (mMediaPlayer.isPlaying())
			return;

		try {
			mMediaPlayer
					.setDataSource("http://digitalabo.besaba.com/speech/01%20song.mp3");

			// mMediaPlayer
			// .setDataSource("https://googledrive.com/host/0Bzu-wTWmWUdRNzd0OHJESV9rTzA/mp3/title 1/01 song.mp3");
			mMediaPlayer.prepare();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void playPauseAudio() {
		if (mMediaPlayer.isPlaying()) {
			mMediaPlayer.pause();
		} else {
			mMediaPlayer.start();
		}
	}

	@Override
	public void onProgressChanged(int progress, int max) {
		Calendar maxCal = Calendar.getInstance();
		maxCal.setTimeInMillis(max);

		Calendar progressCal = Calendar.getInstance();
		progressCal.setTimeInMillis(progress);

		mProgressMax.setText(String.format("%02d:%02d:%02d",
				maxCal.get(Calendar.HOUR), maxCal.get(Calendar.MINUTE),
				maxCal.get(Calendar.SECOND)));
		mProgressTime.setText(String.format("%02d:%02d:%02d",
				progressCal.get(Calendar.HOUR),
				progressCal.get(Calendar.MINUTE),
				progressCal.get(Calendar.SECOND)));
	}
}
