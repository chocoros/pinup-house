package com.abo.tools.utils;

import java.security.InvalidParameterException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class MusicListDB {

	private String mPath;
	private Cursor mCursor;

	private static final int DATABASE_VERSION = 1;
	private SQLiteDatabase mDB;
	private SQLiteOpenHelper mHelper;
	private Context mContext;

	public enum Sort {
		ID, TITLE, SPEAKER, AUTHOR, DATE
	}

	private static final class CreateDB implements BaseColumns {
		public static final String TITLE = "title";
		public static final String SPEAKER = "speaker";
		public static final String AUTHOR = "author";
		public static final String DATE = "date";
		public static final String URL = "url";
		public static final String TOTALTIME = "totaltime";
		public static final String _TABLENAME = "music";
		public static final String _CREATE = "create table " + _TABLENAME + "("
				+ _ID + " integer primary key autoincrement, " + TITLE
				+ " text not null , " + SPEAKER + " text not null , " + AUTHOR
				+ " text not null , " + DATE + " datetime not null , " + URL
				+ " text not null , " + TOTALTIME + " integer not null" + ");";
	}

	public MusicListDB(Context context, String path) {
		mPath = path;
		mContext = context;
		if (mPath == null || mContext == null) {
			new InvalidParameterException("context and path is null.");
		}

		mHelper = new SQLiteOpenHelper(mContext, mPath, null, DATABASE_VERSION) {

			@Override
			public void onUpgrade(SQLiteDatabase db, int oldVersion,
					int newVersion) {
				db.execSQL("DROP TABLE IF EXISTS " + CreateDB._TABLENAME);
				onCreate(db);
			}

			@Override
			public void onCreate(SQLiteDatabase db) {
				db.execSQL(CreateDB._CREATE);
			}
		};

		mDB = mHelper.getWritableDatabase();
		refresh(null);
	}

	public void close() {
		mDB.close();
		mDB = null;
		mHelper.close();
		mHelper = null;
	}

	public void refresh(Sort type) {
		try {
			if (mCursor != null)
				mCursor.close();

			String orderBy = null;
			if (type == null || type == Sort.ID) {
			} else if (type == Sort.TITLE) {
				orderBy = CreateDB.TITLE + " ASC";
			} else if (type == Sort.SPEAKER) {
				orderBy = CreateDB.SPEAKER + " ASC";
			} else if (type == Sort.AUTHOR) {
				orderBy = CreateDB.AUTHOR + " ASC";
			} else if (type == Sort.DATE) {
				orderBy = CreateDB.DATE + " ASC";
			}

			mCursor = mDB.query(CreateDB._TABLENAME, null, null, null, null,
					null, orderBy);
			mCursor.moveToFirst();
		} catch (Exception e) {

		}
	}

	public void insert(MusicItem item) {
		ContentValues values = new ContentValues();
		values.put(CreateDB.TITLE, item.title);
		values.put(CreateDB.SPEAKER, item.speaker);
		values.put(CreateDB.AUTHOR, item.author);
		values.put(CreateDB.URL, item.url);
		values.put(CreateDB.TOTALTIME, item.totalTime);

		// date
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
				Locale.getDefault());
		Date date = new Date(item.date);
		values.put(CreateDB.DATE, dateFormat.format(date));

		mDB.insert(CreateDB._TABLENAME, null, values);
	}

	public void delete(int id) {
		mDB.delete(CreateDB._TABLENAME, CreateDB._ID + " = " + id, null);
	}

	public void deleteAll() {
		mDB.delete(CreateDB._TABLENAME, null, null);
	}

	public void update(MusicItem item) {

	}

	public MusicItem get(int id) {
		if (mCursor == null || !mCursor.moveToPosition(id))
			return null;

		MusicItem item = new MusicItem();
		item.id = mCursor.getInt(mCursor.getColumnIndex(CreateDB._ID));
		item.title = mCursor.getString(mCursor.getColumnIndex(CreateDB.TITLE));
		item.speaker = mCursor.getString(mCursor
				.getColumnIndex(CreateDB.SPEAKER));
		item.author = mCursor
				.getString(mCursor.getColumnIndex(CreateDB.AUTHOR));
		item.totalTime = mCursor.getInt(mCursor
				.getColumnIndex(CreateDB.TOTALTIME));
		return item;
	}

	public int getCount() {
		return mCursor != null ? mCursor.getCount() : 0;
	}
}
