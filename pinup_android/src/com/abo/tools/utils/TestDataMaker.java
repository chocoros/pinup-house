package com.abo.tools.utils;

import java.security.InvalidParameterException;
import java.util.Calendar;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.format.Time;

public class TestDataMaker {

	public interface OnResultListener {
		public void onFinished(String dbPath, int count);
	}

	private String mDBPath;
	private OnResultListener mListener;

	public TestDataMaker(String dbPath, OnResultListener listener) {
		mDBPath = dbPath;
		mListener = listener;

		if (mDBPath == null || mListener == null) {
			new InvalidParameterException("path and listener must not be null.");
		}
	}

	public void makeAsync(final Context context, int count) {
		AsyncTask<Integer, Void, Integer> task = new AsyncTask<Integer, Void, Integer>() {

			@Override
			protected Integer doInBackground(Integer... params) {
				int count = params[0];

				MusicListDB db = new MusicListDB(context, mDBPath);
				MusicItem lastItem = db.get(db.getCount() - 1);
				int id = lastItem != null ? lastItem.id : 0;
				for (int i = 0; i < count; i++) {
					db.insert(makeMusicItem(id + i));
				}

				db.close();
				return count;
			}

			@Override
			protected void onPostExecute(Integer result) {
				super.onPostExecute(result);

				mListener.onFinished(mDBPath, result);
			}

		};

		task.execute(count);
	}

	private MusicItem makeMusicItem(int index) {
		MusicItem item = new MusicItem();
		item.id = index;
		item.title = String.format("%d Great Songs in 1980s", index);
		item.author = String.format("Studio No. %d", index % 20);
		item.url = "https://docs.google.com/spreadsheets/d/1SrSf68NKXY-EZhR-fP0pd6j5sJ3N9XLE6Rkh6DN9N94/edit?pli=1#gid=1725265356";
		item.speaker = String.format("Speaker No. %d", index % 100);
		item.date = Calendar.getInstance().getTimeInMillis();
		item.totalTime = 18000;
		return item;
	}

	public void deleteAsync(final Context context) {
		AsyncTask<Void, Void, Integer> task = new AsyncTask<Void, Void, Integer>() {

			@Override
			protected Integer doInBackground(Void... params) {
				MusicListDB db = new MusicListDB(context, mDBPath);
				int count = db.getCount();
				db.deleteAll();
				db.close();
				return count;
			}

			@Override
			protected void onPostExecute(Integer result) {
				super.onPostExecute(result);
				mListener.onFinished(mDBPath, result);
			}

		};

		task.execute();
	}

}
