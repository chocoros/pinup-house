package com.abo.tools.utils;

public class MusicItem {

	public int id;
	public String title = "";
	public String speaker = "";
	public String author = "";
	public String url = "";
	public long date;
	public long totalTime;
}
