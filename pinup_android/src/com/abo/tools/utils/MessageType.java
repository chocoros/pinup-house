package com.abo.tools.utils;

public class MessageType {

	// internal message
	public static final int FAILED = 0;
	public static final int SUCCESS = 1;
	
	// for message handler
	public static final int STOPSPLASH = 0;
}
