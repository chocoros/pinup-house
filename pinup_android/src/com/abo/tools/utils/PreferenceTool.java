package com.abo.tools.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceTool {

	private final static String FILE_NAME = "settings";

	public final static String ACCOUNT_NAME = "ACCOUNT_NAME";

	public static void setAccountName(Context context, String accountName) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				FILE_NAME, Context.MODE_PRIVATE);

		Editor edit = sharedPreferences.edit();
		edit.putString(ACCOUNT_NAME, accountName);
		edit.commit();
	}

	public static String getAccountName(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				FILE_NAME, Context.MODE_PRIVATE);

		String name = sharedPreferences.getString(ACCOUNT_NAME, "");
		return name;
	}
}
