package com.abo.tools.musicplayer;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class PlaybarMonitor implements OnSeekBarChangeListener {

	public interface OnProgressListener {
		public void onProgressChanged(int progress, int max);
	}

	private SeekBar seekBar;
	private MediaPlayer mediaPlayer;
	private OnProgressListener onProgressListener;
	private ScheduledFuture scheduledFuture;
	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			monitor();
		}
	};

	public PlaybarMonitor(SeekBar seekBar, MediaPlayer mediaPlayer,
			OnProgressListener onProgressListener) {
		this.seekBar = seekBar;
		this.mediaPlayer = mediaPlayer;
		this.onProgressListener = onProgressListener;
		init();
	}

	private void init() {
		ScheduledExecutorService service = Executors.newScheduledThreadPool(1);

		scheduledFuture = service.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {

				handler.sendMessage(handler.obtainMessage(0));
			}
		}, 200, 200, TimeUnit.MILLISECONDS);

		seekBar.setOnSeekBarChangeListener(this);
	}

	private void monitor() {
		try {
			if (mediaPlayer.isPlaying()) {
				int duration = mediaPlayer.getDuration();
				int position = mediaPlayer.getCurrentPosition();

				seekBar.setMax(duration);
				seekBar.setProgress(position);

				if (onProgressListener != null) {
					onProgressListener.onProgressChanged(position, duration);
				}
			}
		} catch (Exception e) {

		}
	}

	public void cancel() {
		scheduledFuture.cancel(true);
		handler.removeMessages(0);
	}

	private void seekTo(int msec) {
		try {
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.seekTo(msec);
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
		if (arg2) {
			seekTo(progress);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar arg0) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar arg0) {
	}
}
