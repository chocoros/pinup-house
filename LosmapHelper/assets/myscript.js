var topNodeABO = 0;
var currentNode;
var currentNodeABO = 0;
var totalNumber = 0;

var doneLosChartShow = false;
var waitForLosChartShow = false;

function getPINLevel(point) {
	if (point < 200000)
		return '0%';
	else if (point < 600000)
		return '3%';
	else if (point < 1200000)
		return '6%';
	else if (point < 2400000)
		return '9%';
	else if (point < 4000000)
		return '12%';
	else if (point < 6800000)
		return '15%';
	else if (point < 10000000)
		return '18%';
	else if (point < 20000000)
		return '21%';
	else
		return 'RUBY';
	
	return 'undefined';
}

function expandAndShowAll()
{
	console.log('[JS][expandAndShowAll]');
	$('#LOSChart').hide();
	doneLosChartShow = false;
	currentNode = $('#treeList');
	showLOSChartAndExpand();
	console.log('[JS][~expandAndShowAll]');
}

function fakeClick(anchorObject)
{
	var clickEvent = document.createEvent('HTMLEvents');
	clickEvent.initEvent('click', true, true);
	anchorObject.dispatchEvent(clickEvent);
	
	var StrLosChartShow = 'LosChartShow';
	var href = $(anchorObject).attr('href');
	var index = href.indexOf(StrLosChartShow);
	//console.log('[JS][fakeClick] index : ' + index + ', href : ' + href);
	if (index >= 0) {
		index += StrLosChartShow.length;
		index += 2; // (' 를 제거
		currentNodeABO = href.substr(index, 8).trim().replace(/[^0-9]/g, '');
		console.log('[JS] currentNodeABO : ' + currentNodeABO);
	}
	
	if (totalNumber == 1) {
		topNodeABO = currentNodeABO;
	}
}

function showLOSChartAndExpand()
{
	if ($('#LOSChart').css('display') == 'block'
		&& $('#chart02').text() != ''
		&& !$(currentNode).hasClass('treePlus'))
	{
		waitForLosChartShow = false;
		setInformation();
	}
	
	if (waitForLosChartShow == false)
	{
		totalNumber++;
		var liTag = findNextNode(currentNode);
		if (liTag == null) {
			setTotalABO();
			$('#LOSChart').hide();
			alert('완료');
			return;
		}

		// init pv value
		$('#LOSChart').hide();
		
		var aTag = $(liTag).find('a')[0];
		fakeClick(aTag);
		
		waitForLosChartShow = true;
		currentNode = liTag;
	}

	setTimeout(function() {
		showLOSChartAndExpand();
	}, 200);
}

function setTotalABO()
{
	var html = $('#pv_' + topNodeABO).html();
	$('#pv_' + topNodeABO).html(html + '&nbsp;&nbsp;&nbsp;&nbsp;회원수:' + totalNumber);	
}

function setInformation()
{
	var abo = currentNodeABO;
	var pv = $('#chart02').text();
	var pvNum = Number(pv.trim().replace(/[^0-9]/g, ''));
	var pin = getPINLevel(pvNum);
	$('#pv_' + abo).html('&nbsp;&nbsp;&nbsp;&nbsp;' + pin + '&nbsp;&nbsp;&nbsp;&nbsp;PV : ' + pv);	
	//console.log('[JS][setInformation] abo : ' + abo + ', PIN : ' + pin + ', PV : ' + pv);
}

function findNextNode(node)
{
	// node가 ul 인 경우 첫번째 li를 리턴
	var firstLi = $(node).children('li:first')[0];
	if (firstLi) {
		return firstLi;
	}

	// node가 li 인 경우 ul 자식 중에 첫번째 li를 리턴
	var ulNode = $(node).children('ul')[0];
	if (ulNode) {
		firstLi = $(ulNode).children('li:first')[0];
		if (firstLi) {
			return firstLi;
		}
	}
	
	// 없다면 next sibling 찾아서 리턴
	var nextSibling = $(node).next();
	if (nextSibling != null && $(nextSibling).is('li')) {
		return nextSibling;
	}
	
	var parent = node;
	while (true) {
		// 없다면 부모의 다음 Li를 찾아서 리턴
		var parent = $(parent).closest('ul');
		if ($(parent).is('#treeList')) {
			console.log('[JS][findNextNode] finish at treeList');
			return null;
		}
		
		parent = $(parent).closest('li')
		var nextLiNodeOfParentLi = $(parent).next();
		if (nextLiNodeOfParentLi != null && $(nextLiNodeOfParentLi).is('li')) {
			return nextLiNodeOfParentLi;
		}
	}
	
	console.log('[JS][findNextNode] finish, no more');
	return null;
}
