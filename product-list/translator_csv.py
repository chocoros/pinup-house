# translator_csv.py
import sys
import os

def usage():
	print("""
Usage
=====
python brand_name file_count
		""")

def oneline(line):
	return line[:-1]

def make_header_csv(output):
	outfile = open(output, 'a')
	header = "NAME,ID,VPS,PV,BV,PRICE,BRAND,CATEGORY 1,CATEGORY 2,CATEGORY 3\n"
	outfile.write(header)
	outfile.close()

def make_csv(input, output):
	infile = open(input, 'r')
	outfile = open(output, 'a')
	brand = oneline(infile.readline())
	category = oneline(infile.readline())

	name_line = oneline(infile.readline())
	info_line = oneline(infile.readline())
	empty_line = oneline(infile.readline())

	while name_line and info_line:
		info_line = info_line.replace(",", "")
		info_line = info_line.replace("\t", ",")
		info_line = info_line.replace(",,,", "")
		line = "%s,%s,%s,%s\n" % (name_line, info_line, brand, category)

		#print(line)
		outfile.write(line)
		name_line = oneline(infile.readline())
		info_line = oneline(infile.readline())
		empty_line = oneline(infile.readline())

	infile.close()
	outfile.close()

if not sys.argv[2:]:
	usage()
	sys.exit()

name = sys.argv[1]
count = sys.argv[2]
print ("brand : %s, %s files" % (name, count))

outfilename = "product_list.csv"
if not os.path.isfile(outfilename):
	make_header_csv(outfilename)

for i in range(1, int(count)+1):
	filename = "%s_%d.txt" % (name, i)
	make_csv(filename, outfilename)



