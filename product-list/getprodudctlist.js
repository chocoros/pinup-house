function clearProductList() {
    localStorage.removeItem("prodAllList");
    localStorage.removeItem("prodAllVPSList");
    localStorage.removeItem("totalProductList");
}

function extractProductList() {
    var prodAllList = {};
    var prodAllVPSList = { list : new Array() };
    if (localStorage.prodAllList != null) {
        prodAllList = JSON.parse(localStorage.prodAllList);
    }
    if (localStorage.prodAllVPSList != null) {
        prodAllVPSList = JSON.parse(localStorage.prodAllVPSList);
    }

    var proListWrap = document.getElementById("proListWrap");
    var proListType = proListWrap.querySelector(".proListType");
    var liList = proListType.getElementsByTagName("li");
    for (var i = 0; i < liList.length; i++) {
        var li = liList[i];

        var img = li.querySelector(".thumbLink").querySelector("a").querySelector("img");
        var dataBinding = li.querySelector("#dataBinding");
        var item = {};
        item["NAME"] = dataBinding.querySelector("#name").innerText;
        item["SKU"] = dataBinding.querySelector("#code").innerText;     // ID
        item["VPS"] = img.alt.substring(1, 5);                          // "[2276]110607K3이름"
        item["PV"] = dataBinding.querySelector("#pv").innerText;
        item["BV"] = dataBinding.querySelector("#bv").innerText;
        item["PRICE"] = dataBinding.querySelector("#price").innerText;    
        item["BRAND"] = dataBinding.querySelector("#brand").innerText;
        item["SUBBRAND"] = dataBinding.querySelector("#subBrand").innerText;    // CATEGORY 1
        item["IMAGESRC"] = img.src;
        prodAllList[item.VPS] = item;
        prodAllVPSList.list.push(item.VPS);
    }

    prodAllVPSList.list.sort(function(left, right) {
        return left - right; 
    });

    localStorage.prodAllList = JSON.stringify(prodAllList);
    localStorage.prodAllVPSList = JSON.stringify(prodAllVPSList);
}

function printProductList() {
    var prodAllList = {};
    var prodAllVPSList = { list : new Array() };
    if (localStorage.prodAllList) {
        prodAllList = JSON.parse(localStorage.prodAllList);
    }
    if (localStorage.prodAllVPSList) {
        prodAllVPSList = JSON.parse(localStorage.prodAllVPSList);
    }

    var totalProductList = "";
    totalProductList += "NAME, SKU, VPS, PV, BV, PRICE, BRAND, CATEGORY 1, CATEGORY 2, CATEGORY 3, AliasName, IMAGESRC\n";
    
    prodAllVPSList.list.forEach(function (element, index, array) {
        var item = prodAllList[element];
        var line = "";
        
        line += item.NAME;
        line += ", ";

        line += item.SKU;
        line += ", ";

        line += item.VPS;
        line += ", ";

        line += item.PV;
        line += ", ";

        line += item.BV;
        line += ", ";

        line += item.PRICE;
        line += ", ";

        line += item.BRAND;
        line += ", ";

        line += item.SUBBRAND;
        line += ", ";

        // CATEGORY 2
        line += ", ";
        // CATEGORY 3
        line += ", ";
        // AliasName
        line += ", ";

        line += item.IMAGESRC;
        line += "\n";
        totalProductList += line;
    });
    localStorage.totalProductList = totalProductList;
    console.log(localStorage.totalProductList);
}

function startProductList() {
    clearProductList();
    extractProductList();    
}

function continueProductList() {
    extractProductList();    
}

//printProductList();
